
# import
# ------------------------------------------------------------------------------------------
import easygui, sys
from multiprocessing import freeze_support
# miam import
import miamQT
import WF_test_
import myapp
import miamResults4PublicationLIGHT
import miamResults4PublicationPALETTE
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
def main():
    # MIAM MAIN CHOICE
    msg ="MIAM: main menu select action"
    title = "MIAM - Remi Cozot (c) 2020"    
    choices = [ "load image (SDR): display image, Lightness Histogram, Palette.",   # [0] load image (SDR): display image, Lightness Histogogram, Palette
                "launch QT interafce."]                                             # [1] launch QT interafce

    # take into account command line arguments
    if len(sys.argv[1:])>=1:
        # sys.argv[0]) == name of module
        if (sys.argv[1]).isnumeric() : 
            choice = choices[int(sys.argv[1])]
            #  according to choice
            print("MIAM:"+choice)
            if choice == choices[0]: myapp.guiImage()
            elif choice == choices[1]: miamQT.startGUI()
            else:
                if choice: print("MIAM:" + choice + " unknown !")
                else: sys.exit(0)
        elif 'help' in sys.argv[1]:
            print("------            MIAM - Remi Cozot (c) 2020              ------")
            print("----------------------------------------------------------------")
            print("possible choices:")
            for i, c in enumerate(choices):
                print("python __miam.py ",i, ">",choices[i])
            print("----------------------------------------------------------------")
            sys.exit(0)
        else:
            choice = "error command line parameter: "+ sys.argv[1]

    else: # no parameter in command line (interactive mode)
        cont = True
        while cont:
            choice = easygui.choicebox(msg, title, choices)
            if choice:
                print("MIAM:"+choice)
            else: sys.exit(0)

            #  according to choice
            if choice == choices[0]: myapp.guiImage()
            elif choice == choices[1]:  miamQT.startGUI()

            # continue or stop
            msgRedo = "Do you want to continue?"
            titleRedo = "Please Confirm"
            cont =  easygui.ccbox(msgRedo, titleRedo)

    # miamResults4PublicationPALETTE.clusterPalette()
    # miamResults4PublicationLIGHT.clusterLightnessHist()
    # myapp.guiSDR()
# ------------------------------------------------------------------------------------------
if __name__ == '__main__':
    freeze_support()
    main()
# ------------------------------------------------------------------------------------------
