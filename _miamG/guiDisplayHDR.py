# import
# ------------------------------------------------------------------------------------------
import matplotlib.pyplot as plt
import numpy as np
import easygui
import os

# miam import
import miam.image.Image as MIMG
import miam.processing.TMO_Lightness as TMO_L
import miam.histogram.Histogram  as MHIST
import miam.utils

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pathImage = easygui.fileopenbox(msg="select HDR image.")
    (path, name) =os.path.split(pathImage)
    print("selected image:", pathImage, "name:", name)

    # load image and remove zeros
    img = MIMG.Image.readImage(pathImage)
    img.name = name   
    img = img.removeZeros().removeZeros(0.5)
    img.name = name   
    print("dynamic range (0.5%):",img.getDynamicRange( percentile = 0.0, mode="f-stops"),"f-stops")
    hY = MHIST.Histogram.build(img,MIMG.channel.channel.Y,nbBins=50)

    # tonemap
    tmImg = img.process(TMO_L.TMO_Lightness())
    tmImg.name = img.name+"Lightness_TMO"
    htmL = MHIST.Histogram.build(tmImg,MIMG.channel.channel.L,nbBins=50)

    # sve tone mapped image
    tmImg.writeImage("../images/"+tmImg.name.split('.')[0]+"_Lightness_TMO")

    # plot
    fig, ax = plt.subplots(2,2)
    img.plot(ax[0,0])
    hY.plot(ax[0,1])
    tmImg.plot(ax[1,0])
    htmL.plot(ax[1,1])
    plt.show(block=True)
    
    # just Y
    #import miam.processing.ColorSpaceTransform as MCST
    #Y = MCST.ColorSpaceTransform().compute(tmImg,dest='XYZ')
    #plt.figure()
    #plt.imshow(Y.colorData[:,:,1],cmap='gray')
    #plt.show(block=True)