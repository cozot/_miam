import miam.utils
import miam.image.Image as MIMG
import miam.processing.TMO_Lightness as TMO_L
import miam.processing.ContrastControl as PCC
import imageio

def tmo_Lightness_localEQ(hdrImageFilename):
        (name, ext) = miam.utils.splitFileName(file)

        # load image and remove zeros
        img = MIMG.Image.readImage(srcPath+file).removeZeros().removeZeros(0.5) 

        # tonemap
        res = img.process(TMO_L.TMO_Lightness())
      
        # contrast control
        imgLE = res.process(PCC.ContrastControl(),method='localEqualization', size=1500)

        # fusion
        alpha = 0.75
        fusion = alpha*res+(1-alpha)*imgLE
 
        # save 
        imageio.imsave(name+"_Lightness_localEQ.jpg",fusion.colorData)
        print("ok", end='\n')
