# import
# ------------------------------------------------------------------------------------------
import os, time
import matplotlib.pyplot as plt
import numpy as np
import multiprocessing as mp

import easygui
import imageio

# miam import
import miam.image.Image as MIMG
import miam.image.imageType as MTYPE
import miam.image.channel as MCH

import miam.processing.ColorSpaceTransform as MCST
import miam.processing.TMO_Lightness as TMO_L
import miam.processing.ContrastControl as PCC
import miam.histogram.Histogram  as MHIST

import miam.aesthetics.LightnessAesthetics as MLAC
import miam.aesthetics.Palette as MPAL
import miam.aesthetics.Composition as MCMP

import miam.imageDB.ImageDB
import miam.imageDB.POGChecker
import miam.imageDB.HwHDRBuilder
import miam.html.generator
import miam.utils

import miam.pointcloud.PointCloud2D as MPC2D
import miam.classification.kmeans
import miam.math.Distance 
import miam.math.Normalize 

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------


# --------------------------------------------> basic app
def guiImage(filename=None):
    if not filename:
        filename = easygui.fileopenbox(msg="select image.")
        print("MIAM[selected image:", filename,"]")

    # paramters
    nbColorInPalette =5
    nbPointInCompo =10
    # reading image
    start = time.time()
    image = MIMG.Image.readImage(filename, readExif=False)
    end = time.time()
    print("[read image: ........ ",int((end-start)*1000),"(ms)]")
    # compute histogram

    start = time.time()
    histogram = MHIST.Histogram.build(image,MIMG.channel.channel.L,50).normalise(norm='dot')
    end = time.time()
    print("[compute histogram: . ",int((end-start)*1000),"(ms)]")

    # compute palette
    start = time.time()
    palette = MPAL.Palette.build(image,nbColorInPalette,method='kmean-Lab', removeBlack=True)
    imageOfPalette = palette.createImageOfPalette()    
    end = time.time()
    print("[compute palette: ... ",int((end-start)*1000),"(ms)]")    

    # compute composition
    start = time.time()
    composition = MCMP.Composition.build(image,nbPoint =nbPointInCompo, nbZone=9)
    end = time.time()
    print("[compute composition: ",int((end-start)*1000),"(ms)]")


    # display all elements
    fig = plt.figure()

    axHisto =   plt.subplot(233)
    axPalette = plt.subplot(236)
    axImage =   plt.subplot(131)
    axCompo =   plt.subplot(132)

    image.plot(axImage,title=False)
    histogram.plot(axHisto,title=False)
    imageOfPalette.plot(axPalette, title=False)
    image.plot(axCompo,title=False)
    composition.plot(axCompo,title=False, keepRawPoints = True)

    axHisto.set_title("Histogram (Lightness)")
    axPalette.set_title("Palette ("+str(nbColorInPalette)+" colors)")
    axImage.set_title("Image")
    axCompo.set_title("Composition ("+str(nbPointInCompo)+" points)")
    fig.suptitle(image.name)


    plt.show(block=True)

def guiHDR():
    pathImage = easygui.fileopenbox(msg="select HDR image.")
    print("selected image:", pathImage)

    # load image and remove zeros
    img = MIMG.Image.readImage(pathImage).removeZeros().removeZeros(0.5)
    ((minR,maxR),(minG,maxG),(minB,maxB)) = img.getMinMaxPerChannel()
    # print min max per channel
    print("(min/max): R:(",minR,"/",maxR,") - G:(",minG,"/",maxG,") -B:(",minB,"/",maxB,")")


    # to XYZ
    imgXYZ = MCST.ColorSpaceTransform().compute(img,dest='XYZ')
    # print min max per channel
    ((minX,maxX),(minY,maxY),(minZ,maxZ)) = imgXYZ.getMinMaxPerChannel()
    print("(min/max): X:(",minX,"/",maxX,") - Y:(",minY,"/",maxY,") - Z:(",minZ,"/",maxZ,")")

    # dynamic range
    print("dynamic range (0.5%):",img.getDynamicRange( percentile = 0.25, mode="f-stops"),"f-stops/",
          img.getDynamicRange( percentile = 0.25, mode="maxmin"),"(Ymax,Ymin)")
    print("dynamic range (0):",img.getDynamicRange( percentile = 0, mode="f-stops"),"f-stops/",
          img.getDynamicRange( percentile = 0, mode="maxmin"),"(Ymax,Ymin)")
    # histogram
    hRGB = MHIST.Histogram.build(img,MIMG.channel.channel.sG,nbBins=50)
    hXYZ = MHIST.Histogram.build(imgXYZ,MIMG.channel.channel.Y,nbBins=50)

    # plot
    fig, ax = plt.subplots(1,3)
    img.plot(ax[0])
    hRGB.plot(ax[1])
    hXYZ.plot(ax[2])
    plt.show(block=True)

# --------------------------------------------> Lightness HDR dataset
def testDB_Huawei():
    hDB = miam.imageDB.ImageDB_HDD.ImageDB_HDD(name= "HDR DataBase" ,csvFile="../DB/hdrHuawei_DB.csv", jsonConfigFile ="../DB/config_HDR_DB.json")
    hDB.build(miam.imageDB.HwHDRBuilder.HwHDRBuilder(),src='HDR')
    #hDB.build(miam.imageDB.HwHDRBuilder.HwHDRBuilder(),src='HTM')
    #hDB.check()
    hDB.buildHTMLPage()
    
# --------------------------------------------> Lightness TMO
def guiLightnessTMO():
    pathImage = easygui.fileopenbox(msg="select HDR image.")
    print("selected image:", pathImage)

    # load image and remove zeros
    img = MIMG.Image.readImage(pathImage).removeZeros().removeZeros(0.5) #.resize((1920,None))

    # tonemap
    res = img.process(TMO_L.TMO_Lightness())

    # contrast control
    imgLE = res.process(PCC.ContrastControl(),method='localEqualization', size=1500)
    imgLE.name += '_local equaliz.'
    

    alpha = 0.75
    fusion = alpha*res+(1-alpha)*imgLE
    fusion.name = 'fusion'

    # histograms
    #hHDR = MHIST.Histogram.build(img,MIMG.channel.channel.Y,nbBins=50)      # hdr image histogram
    #hTMoL = MHIST.Histogram.build(res,MIMG.channel.channel.L,nbBins=50)     # TMo lightness histogram
    #hLE = MHIST.Histogram.build(imgLE,MIMG.channel.channel.L,nbBins=50)     # TMo Lightness + local equalization histogram

    # plot
    fig, ax = plt.subplots(2,2)
    img.plot(ax[0,0])   # hdr
    res.plot(ax[0,1])   # lightness tone mapping

    fusion.plot(ax[1,0])
    imgLE.plot(ax[1,1])

    plt.show(block=True)
    imageio.imsave("testLightness_loalEQ.jpg",fusion.colorData)
    #imageio.imsave("testLightness+stretch.jpg",imgCS.colorData)
    #imageio.imsave("testLightness+equalization.jpg",imgGE.colorData)
    pass

def testLightnessTMO():
    srcPath = "../../../photos/HDR-DB/HDR/"
    listFiles = os.listdir(srcPath)
    for file in listFiles:
        print("fileName:", file , end='::')
        (name, ext) = miam.utils.splitFileName(file)
        print("loading", end='::')
        # load image and remove zeros
        img = MIMG.Image.readImage(srcPath+file).removeZeros().removeZeros(0.5) 
        print("ok", end='::')
        print("tone mapping", end='::')
        # tonemap
        res = img.process(TMO_L.TMO_Lightness())
        print("ok", end='::')
        print("post-processing", end='::')        
        # contrast control
        imgLE = res.process(PCC.ContrastControl(),method='localEqualization', size=1500)
        print("ok", end='::')
        print("fusion", end='::') 
        # fusion
        alpha = 0.75
        fusion = alpha*res+(1-alpha)*imgLE
        print("ok", end='::')
        print("save", end='::') 
        # save 
        imageio.imsave(name+"_Lightness_localEQ.jpg",fusion.colorData)
        print("ok", end='\n')

# --------------------------------------------> Segmentation
def testSegment(filename=None):
    if not filename:
        filename = easygui.fileopenbox(msg="select HDR image.")
    
        print("selected image:", filename)

    # reading image
    i0 = MIMG.Image.readImage(filename)
    
    # compute histogram
    h0 = MHIST.Histogram.build(i0,MIMG.channel.channel.L,100).normalise(norm='probability')
    print(h0)

    res = h0.segmentPics(nbSegs=5)

def testMask(filename=None):
    if not filename:
        filename = easygui.fileopenbox(msg="select HDR image.")
    
        print("selected image:", filename)

    # reading image
    i0 = MIMG.Image.readImage(filename)
    
    mask = i0.buildMaskByValue(MIMG.channel.channel.L, 0,50)

    fig, ax = plt.subplots(nrows=1, ncols=2)
    # +-----------------------------+-----------------------------+
    # | [0]= i0                     | [1]=  mask                  | 
    # +-----------------------------+-----------------------------+

    i0.plot(ax[0])
    mask.plot(ax[1])
    plt.show(block=True)

def testSegment2(filename=None):
    if not filename:
        filename = easygui.fileopenbox(msg="select an image.")
    
        print("selected image:", filename)
        None
    # reading image and compute histogram
    i0 = MIMG.Image.readImage(filename)

    if i0.type == MTYPE.imageType.HDR:
        LorY = MIMG.channel.channel.Y
        i0 = i0.removeZeros(0.5)
    else:
        LorY = MIMG.channel.channel.L

    h0 = MHIST.Histogram.build(i0,LorY,100).normalise(norm='probability')

    segmentBoundariesValue = h0.segmentPics(nbSegs=5)

    # debug
    print("segment boundaries value:", segmentBoundariesValue)
    mask_i = []
    for i in range(len(segmentBoundariesValue)-1):
        mask = i0.buildMaskByValue(LorY,segmentBoundariesValue[i],segmentBoundariesValue[i+1])
        mask_i.append(mask)
    
    # plot
    fig, ax = plt.subplots(nrows=1, ncols=1)
    # +-----------------------------+
    # | [0]= i0                     | 
    # +-----------------------------+
    i0.plot(ax)
    plt.show(block=False)

    # plot
    fig, ax = plt.subplots(nrows=len(segmentBoundariesValue)-1, ncols=1)
    # +-----------------------------+
    # | [i]=mask[i]                 | 
    # +-----------------------------+
    for i in range(len(segmentBoundariesValue)-1):
        mask_i[i].plot(ax[i])
    plt.show(block=True)

# --------------------------------------------> Palette
def testAddPalette(filename0=None, filename1=None):

    # select images
    if not filename0:
        filename0 = easygui.fileopenbox(msg="select first image.")
        print("selected image:", filename0)
    if not filename1:
        filename1 = easygui.fileopenbox(msg="select first image.")
        print("selected image:", filename1)

    # reading image
    i0 = MIMG.Image.readImage(filename0)
    i1 = MIMG.Image.readImage(filename1)

    # compute palettes
    p0 = MPAL.Palette.build(i0,5,method='kmean-Lab', removeBlack=True)
    imageOfPalette0 = p0.createImageOfPalette()
    p1 = MPAL.Palette.build(i1,5,method='kmean-Lab', removeBlack=True)
    imageOfPalette1 = p1.createImageOfPalette()

    # average palette
    p01 = 0.5*(p0+p1)

    fig, ax = plt.subplots(nrows=2, ncols=3)
    # +-----------------------------+-----------------------------+-----------------------------+
    # | [0,0]= i0                   | [0,1]=  i1                  | [0,2]= p01                  |
    # +-----------------------------+-----------------------------+-----------------------------+
    # | [1,0]= p0                   | [1,1]= p1                   | [1,2]=                      |
    # +-----------------------------+-----------------------------+-----------------------------+
    i0.plot(ax[0,0])
    imageOfPalette0.plot(ax[1,0])

    i1.plot(ax[0,1])
    imageOfPalette1.plot(ax[1,1])

    p01.createImageOfPalette().plot(ax[0,2])

    plt.show(block=True)

# --------------------------------------------> Compistion
def estimateComposition(filename=None):
    if not filename:
        filename = easygui.fileopenbox(msg="select image.")
        print("selected image:", filename)

    # reading image
    img = MIMG.Image.readImage(filename)

    comp = MCMP.Composition.build(img,nbPoint =3, nbZone=9)

    fig, ax = plt.subplots(nrows=1, ncols=2)
    # +-----------------------------+-----------------------------+-----------------------------+
    # | [0,0]= image                | [0,1]=  ssl map             | [0,2]=                      |
    # +-----------------------------+-----------------------------+-----------------------------+
    # | [1,0]=                      | [1,1]=                      | [1,2]=                      |
    # +-----------------------------+-----------------------------+-----------------------------+
    img.plot(ax[0])
    comp.plot(ax[0],title=False)
    ax[0].axis(False)

    comp.plot(ax[1])

    plt.show(block=True)
    # load aesthetics classes
    #centroidsAES = MLAC.LightnessAesthetics.readLightnessClasses('../../aestheticsClass/both/statsLY/'+'km_centroids_nbCluster5_nbBin50.npy')
    #dists = centroidsAES.computeImageClass(i0)

    #h0AES0 = centroidsAES.projectImage(i0,nbClass=range(0,1))
    #h0AES01 = centroidsAES.projectImage(i0,nbClass=range(0,2))



