# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import easygui
import colour

# import Qt
from PyQt5.QtWidgets import QApplication

# miam import

# new import
import gui.guiController.MainWindowController as MWC

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

def startGUI():
    app = QApplication(sys.argv)
    # start MainWindowController
    mwc = MWC.MainWindowController(app)
    sys.exit(app.exec_())
