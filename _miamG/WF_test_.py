# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import numpy as np
import easygui
import colour

# miam
import miam.workflow.WFWorkflow as WF
import miam.workflow.WFProcess as WP
from miam.processing import NoOp, Duplicate,ExposureControl,TMO_Linear, TMO_CCTF
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

def WF_test_00():

    #                    (o) Root
    #                     |
    #                     v
    #               [     o     ]
    #               [    NoOp   ]
    #               [     o     ]
    #                     |
    #                     v
    #                    (_) Leaf
    # process
    wf = WF.WFWorkflow(name="test_00")
    p1 = wf.addProcess(WP.WFProcess(name = 'NOP_01', process=NoOp.NoOp()))
    p2 = wf.addProcess(WP.WFProcess(name = 'NOP_02', process=NoOp.NoOp()))
    wf.connect(outputProcess = wf.getByName('NOP_01'),
               inputProcess  = p2
               )
    wf.compile()


def WF_test_01():

    #                    (o) Root
    #                     |
    #                     v
    #               [     o     ]
    #               [  exposure ]
    #               [     o     ]
    #                     |
    #                     v
    #                    (_) Leaf
    # process
    wf = WF.WFWorkflow(name="test_01")

    exposure = wf.addProcess(WP.WFProcess(name = 'exposure', process=ExposureControl.ExposureControl()).setParameters({'EV': +1}))

    wf.compile()
    wf.compute()    
    wf.display()

def WF_test_02():
    # process
    wf = WF.WFWorkflow(name="test_02")
    duplicate = wf.addProcess(WP.WFProcess(name = 'duplicate', process=Duplicate.Duplicate()).setParameters({'nb': 2}))
    exposureP1 = wf.addProcess(WP.WFProcess(name = 'exposure+1', process=ExposureControl.ExposureControl()).setParameters({'EV': +1}))
    exposureM1 = wf.addProcess(WP.WFProcess(name = 'exposure-1', process=ExposureControl.ExposureControl()).setParameters({'EV': -1}))

    wf.connect(outputProcess = duplicate, inputProcess  = exposureP1)
    wf.connect(outputProcess = duplicate, inputProcess  = exposureM1)

    wf.compile()
    wf.compute()    
    wf.display()

def WF_test_03():

    #                    (o) Root
    #                     |
    #                     v
    #               [     o     ]
    #               [ duplicate ]
    #               [ o   |   o ]
    #                /         \
    #               /           \
    #              v             v
    #     [        o ]         [ o        ]          
    #     [ exposure ]         [ TMO CCTF ]
    #     [    o     ]         [    o     ]
    #          |                    |
    #          v                    v
    #     [    o       ]           (_) Leaf
    #     [ TMO linear ]
    #     [     o      ]
    #           |
    #           v
    #          (_) Leaf
    # process
    wf = WF.WFWorkflow(name="test_03")
    duplicate = wf.addProcess(WP.WFProcess(name = 'duplicate', process=Duplicate.Duplicate()).setParameters({'nb': 2}))
    exposure = wf.addProcess(WP.WFProcess(name = 'exposure', process=ExposureControl.ExposureControl()).setParameters({'auto': True, 'target': 0.5, 'EV': 0}))
    tmo_linear = wf.addProcess(WP.WFProcess(name = 'tmo_linear', process=TMO_Linear.TMO_Linear()).setParameters({'min': 0.0, 'max': 1.0}))
    tmo_cctf = wf.addProcess(WP.WFProcess(name = 'tmo_cctf', process=TMO_CCTF.TMO_CCTF()).setParameters({'function':'sRGB'}))

    wf.setRoot(duplicate)

    wf.connect(outputProcess = duplicate, inputProcess  = exposure)
    wf.connect(outputProcess = exposure, inputProcess  = tmo_linear)
    wf.connect(outputProcess = duplicate, inputProcess  = tmo_cctf)

    wf.setLeaf(tmo_linear)
    wf.setLeaf(tmo_cctf)
              
    wf.compile()

    wf.compute()    
    wf.display()

def WF_JSON():
    jsonFile = "../workflows/wf01.json"
    wf = WF.WFWorkflow.readWorkflow(jsonFile)
    wf.compute()
    wf.display()
