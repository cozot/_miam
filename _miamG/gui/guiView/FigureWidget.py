# import
# ------------------------------------------------------------------------------------------


# import Qt
from PyQt5 import QtCore, QtWidgets


# QT matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure



class FigureWidget(FigureCanvas):
    """
        description of class
    """
    def __init__(self, parent=None, width=5, height=5, dpi=100):
        # create Figure
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        # explicite call of super controller
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.updateGeometry(self)

