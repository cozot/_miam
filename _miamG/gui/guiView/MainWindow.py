# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import multiprocessing as mp
import matplotlib
import numpy as np
import easygui
import colour

# import Qt
from PyQt5.QtWidgets import QMainWindow, QAction, QApplication, QMenu
from PyQt5.QtWidgets import QWidget, QLabel, QFileDialog 
from PyQt5.QtWidgets import QHBoxLayout # QSlider
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5 import QtCore, QtWidgets

# QT matplotlib

# miam import
import miam.image.Image as MIMG
import miam.histogram.Histogram as MHIST
import miam.image.channel
import miam.utils

# gui import
import gui.guiController.MainWindowController as gMWC
import gui.guiView.ImageWidget as gIW
import gui.guiView.FigureWidget as gFW

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------


class MainWindow(QMainWindow):
    """ 
        MainWindow(Vue)
    """
    
    # version 0.1
    # features:
    # load image
    # switch between display mode
    #     
    def __init__(self, controller = None):
        super().__init__()

        # attributes
        self.controller = controller

        # build menu
        self.buildFileMenu()
        self.buildDisplayMenu()
        self.buildWorkflowMenu()

        # setGeometry
        self.setWindowGeometry()
        # title
        self.setWindowTitle('MIAM - Rémi Cozot (c) 2020')   
        # status bar
        self.statusBar().showMessage('Welcome to MIAM: Multidimensional Image Aesthetics Model!')

        # centralWidgets
        self.imageWidgets = []

        self.layout = QHBoxLayout()
        self.container = QWidget()

        #self.setCentralWidget(self.container)

    # methods
    def resizeEvent(self, event):
        if self.controller.displayMode != gMWC.mainWidgetDisplayMode.HIST :
            for cw in self.imageWidgets:
                cw.resize()
        pass

    # build menu
    # ---------------------------------------------------------
    def buildFileMenu(self):
        """ 
           XXX XXX XXX XXX 
        """
        # get menubar
        menubar = self.menuBar()

        # file menu
        fileMenu = menubar.addMenu('&File')

        # Create Open  image action
        openAction = QAction('&Open image', self)        
        openAction.setShortcut('Ctrl+N')
        openAction.setStatusTip('Open image')
        openAction.triggered.connect(self.controller.openImage)
        fileMenu.addAction(openAction)

        fileMenu.addSeparator()

        # Create Open workflow action
        openWFAction = QAction('&Load workflow', self)        
        openWFAction.setShortcut('Ctrl+W')
        openWFAction.setStatusTip('Load worflow')
        openWFAction.triggered.connect(self.controller.openWorkflow)
        fileMenu.addAction(openWFAction)

    def buildDisplayMenu(self):
        """ 
           XXX XXX XXX XXX 
        """
        # get menubar
        menubar = self.menuBar()

        # file menu
        displayMenu = menubar.addMenu('&Display')

        # Create Display Input action
        displayInputAction = QAction('&Image', self)        
        displayInputAction.setShortcut('Ctrl+I')
        displayInputAction.setStatusTip('Display image on/off')
        displayInputAction.triggered.connect(self.controller.displayIMG)
        displayMenu.addAction(displayInputAction)

        # Create Display Current action
        displayCurrentAction = QAction('&Histogram', self)        
        displayCurrentAction.setShortcut('Ctrl+H')
        displayCurrentAction.setStatusTip('Display histogram on/off')
        displayCurrentAction.triggered.connect(self.controller.displayHIST)
        displayMenu.addAction(displayCurrentAction)   

    def buildWorkflowMenu(self):
        """ 
           XXX XXX XXX XXX 
        """
        # get menubar
        menubar = self.menuBar()

        # file menu
        workflowMenu = menubar.addMenu('&Workflow')

        # Create compute workflow action
        computeAction = QAction('&Compute', self)        
        computeAction.setShortcut('Ctrl+C')
        computeAction.setStatusTip('Compute workflow')
        computeAction.triggered.connect(self.controller.compute)
        workflowMenu.addAction(computeAction)
        

    # setWindowGeometry
    def setWindowGeometry(self, scale=0.8):

        width = self.controller.screenSize[0].width()
        height = self.controller.screenSize[0].height()

        # geometry
        self.setGeometry(0, 0, math.floor(width*scale), math.floor(height*scale))

    # setImage
    def setImage(self, imgs):
        # get display mode from controller
        self.controller.dispHDR_linear      # display mode for HDR linear | cctf  

        # reset
        self.images = imgs
        self.imageWidgets = []
        self.layout = QtWidgets.QHBoxLayout()
        self.container = QtWidgets.QWidget()

        for img in self.images:
            # create an imageWidget
            imgW = gIW.ImageWidget(img)
            self.imageWidgets.append(imgW)

            # add ImageWidget
            self.layout.addWidget(imgW)

            if self.controller.displayMode == gMWC.mainWidgetDisplayMode.IMGnHIST:
                # create histogram
                if img.isHDR():
                    ch = miam.image.channel.channel.Y
                else:
                    ch = miam.image.channel.channel.L
                imgHist = MHIST.Histogram.build(img,ch)

                histW = gFW.FigureWidget()
                imgHist.plot(histW.axes)
                self.layout.addWidget(histW)

        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)

    # setDislaytoIMG
    def setDislaytoIMG(self):
        # reset
        self.layout = QtWidgets.QHBoxLayout()
        self.container = QtWidgets.QWidget()
        self.imageWidgets =[]

        for img in self.images:
            # create an imageWidget
            imgW = gIW.ImageWidget(img)
            self.imageWidgets.append(imgW)
            self.layout.addWidget(imgW)

        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)

    # setDislaytoIMGnHIST
    def setDislaytoIMGnHIST(self):
        # reset
        self.layout = QtWidgets.QHBoxLayout()
        self.container = QtWidgets.QWidget()
        self.imageWidgets =[]

        for img in self.images:
            # create an imageWidget
            imgW = gIW.ImageWidget(img)
            self.imageWidgets.append(imgW)
    
            # add ImageWidget
            self.layout.addWidget(imgW)

            # create histogram
            if imgW.image.isHDR():
                ch = miam.image.channel.channel.Y
            else:
                ch = miam.image.channel.channel.L
            imgHist = MHIST.Histogram.build(imgW.image,ch)

            histW = gFW.FigureWidget()
            imgHist.plot(histW.axes)
            self.layout.addWidget(histW)

        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)

    # setDislaytoIMGnHIST
    def setDislaytoHIST(self):
        # reset
        self.layout = QtWidgets.QHBoxLayout()
        self.container = QtWidgets.QWidget()
        self.imageWidgets =[]

        for img in self.images:
            # create an imageWidget
            imgW = gIW.ImageWidget(img)
            self.imageWidgets.append(imgW)

        for imgW in self.imageWidgets:
            # create histogram
            if imgW.image.isHDR():
                ch = miam.image.channel.channel.Y
            else:
                ch = miam.image.channel.channel.L
            imgHist = MHIST.Histogram.build(imgW.image,ch)

            histW = gFW.FigureWidget()
            imgHist.plot(histW.axes)
            self.layout.addWidget(histW)

        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)




