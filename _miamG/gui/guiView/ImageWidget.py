# Qt import
from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt

# import
import numpy as np

# miam import
import miam.image.Image as MIMG

class ImageWidget(QWidget):
    """description of class"""
    def __init__(self,image):
        super().__init__()

        # create a QtLabel for pixmap
        self.label = QLabel(self)

        # image content
        self.image = None
        self.set(image)

    def resize(self):
        self.label.resize(self.size())
        self.label.setPixmap(self.imagePixmap.scaled(self.size(),Qt.KeepAspectRatio))

    def set(self,image):
        # read image
        self.image = image

        # compute pixmap
        height, width, channel = self.image.shape
        bytesPerLine = channel * width
        
        # mask
        colorData =  self.image.colorData*self.image.mask[...,np.newaxis] if self.image.hasMask() else self.image.colorData

        # QImage
        qImg = QImage((colorData*255).astype(np.uint8), width, height, bytesPerLine, QImage.Format_RGB888)
        self.imagePixmap = QPixmap.fromImage(qImg)

        self.resize()

