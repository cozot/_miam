# Qt import


# import
import numpy as np

# miam import
import miam.image.Image as MIMG
import miam.image.imageType as MTYPE
import miam.workflow.WFWorkflow as MWF

class MainWindowModel(object):
    """
    class for main window model
    """

    def __init__(self, controller):

        # attributes
        # ----------------------------------------

        # reference to MainWindowController
        self.controller = controller

        # input image (miam.image.Image)
        # only change when load called
        self.inputImage =  None 

        # list of processes
        self.workflow = None

        # smaller images to faster computation display
        self.image = None

    # method(s)
    # ------------------------------------------------------------------------
    def readImage(self, filename):

        # read image
        # store input : no processing applyed 
        self.inputImage = MIMG.Image.readImage(filename)

        # reset self.image list and self.process
        self.image = None

        # compute smaller image 4 interactive
        width = self.controller.screenSize[0].width()
        self.image = self.inputImage.resize((width,None))
         
        # default processing for HDR iamge
        if self.inputImage.type == MTYPE.imageType.HDR:
            self.image = self.image.removeZeros(0.5)

        return [self.image]

    def readWorkflow(self, filename):
        # read workflow
        # note: read include compile
        self.workflow = MWF.WFWorkflow.readWorkflow(filename)

    # ------------------------------------------------------------------------
    def compute(self):
        # return list of image
        resImgs = []

        if self.workflow:
            # check if input image
            if self.inputImage:
                # compute with self.image for faster computation
                self.workflow.compute(self.image)
                for leaf in self.workflow.leafs:
                    resImgs.append(leaf.image)
        
            else: # no input image
                self.controller.statusMessage("compute: no input image ! Open image first ...")
        else : # no workflow
            self.controller.statusMessage("compute: no workflow ! Load workflow first ...")

        return resImgs
    # ------------------------------------------------------------------------




