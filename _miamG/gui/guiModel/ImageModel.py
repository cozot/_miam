# miam import
import miam.image.Image as MIMG
import miam.image.imageType as MTYPE

class ImageModel(object):
    """description of class"""

    def __init__(self, controller):
        # reference to image controller
        self.controller = controller

        # image gui model

        # a list of miam.image.Image
        # each new process give a new image append to the list self.image
        # the image displayed is self.currentImage
        self.currentImage =None # index of current image
        self.image = []
        # list of 'process'
        # version 0.1: simple dict list
        self.history = []
        pass

    # methods called by controller
    def readImage(self,filename):
        # read image
        self.image.append(MIMG.Image.readImage(filename))
        self.history.append({'readImage':filename})
        # default processing for HDR iamge
        if self.image[-1].type == MTYPE.imageType.HDR:
            self.image.append(self.image[-1].removeZeros(0.5))
            self.history.append({'removeZeros':'0.5'})

        self.currentImage = len(self.image) -1

    def getColorData(self):
        return self.image[self.currentImage].colorData


    def resize(self,size,anti_aliasing=True):
        #resize(self,size=(None,None),anti_aliasing=False)
        pass


