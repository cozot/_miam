# import
import enum, sys

# pyQT5 import
#from PyQt5.QtWidgets import QWidget, QLabel, QFileDialog 
from PyQt5.QtWidgets import QFileDialog, QApplication
# gui import
import gui.guiView.MainWindow as MWView
import gui.guiModel.MainWindowModel as MWModel

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

# local function
def getScreenSize(app):
    screens = app.screens()
    res = list(map(lambda x: x.size(), screens))
    # debug
    print(res)
    return res

# local class
class mainWidgetDisplayMode(enum.Enum):
    """ Enum  """

    IMG         = 0         # single display image
    HIST        = 1         # single display image
    IMGnHIST    = 2         # dual display image and its histogram


class MainWindowController(object):
    """controller for MainWindow"""

    def __init__(self, app):

        # get screens size
        self.screenSize = getScreenSize(app)

        # attributes
        self.displayMode = mainWidgetDisplayMode.IMGnHIST # default display mode
        self.dispHDR_linear = True

        # build vue
        self.view =  MWView.MainWindow(self)
        self.view.show()

        # build model
        self.model = MWModel.MainWindowModel(self)

    # utils methods
    def statusMessage(self, s): self.view.statusBar().showMessage(s)

    # callBack methods
    # --------------------------------------------------------------

    # menu: file

    # open image file
    def openImage(self):
        # status message
        self.statusMessage('open image file ...')
        
        # open file dialog
        fname = QFileDialog.getOpenFileName(None, 'Open file', '../images/')[0]
        self.statusMessage('open image file:'+fname)

        # ask model to load image
        imgs = self.model.readImage(fname)
        self.view.setImage(imgs)

    # open workflow
    def openWorkflow(self):
        # status message
        self.statusMessage('load workflow file ...')
        
        # open file dialog
        fname = QFileDialog.getOpenFileName(None, 'Open file', '../workflows/')[0]
        self.statusMessage('load workflow file:'+fname)

        # ask model to load workflow
        self.model.readWorkflow(fname)

    # menu: workflow

    # compute
    def compute(self):
        # status message
        self.statusMessage('compute ...')
        imgs = self.model.compute()
        self.statusMessage('compute ...'+ str(len(imgs))+' images have benn computed')

        if len(imgs)>0: self.view.setImage(imgs)

    # menu: display

    # image only
    def displayIMG(self):
        if self.displayMode == mainWidgetDisplayMode.IMGnHIST:
            self.statusMessage('display set to histogram ...')
            self.displayMode = mainWidgetDisplayMode.HIST
            self.view.setDislaytoHIST()
        elif self.displayMode == mainWidgetDisplayMode.HIST:
            self.statusMessage('display set to image and histogram ...')
            self.displayMode = mainWidgetDisplayMode.IMGnHIST
            self.view.setDislaytoIMGnHIST()
        elif self.displayMode == mainWidgetDisplayMode.IMG:
            self.statusMessage('...')
            pass
 
    # hist only
    def displayHIST(self):
        if self.displayMode == mainWidgetDisplayMode.IMGnHIST:
            self.statusMessage('display set to image ...')
            self.displayMode = mainWidgetDisplayMode.IMG
            self.view.setDislaytoIMG()
        elif self.displayMode == mainWidgetDisplayMode.HIST:
            self.statusMessage('...')
        elif self.displayMode == mainWidgetDisplayMode.IMG:
            self.statusMessage('display set to image and histogram ...')
            self.displayMode = mainWidgetDisplayMode.IMGnHIST
            self.view.setDislaytoIMGnHIST()
            pass

