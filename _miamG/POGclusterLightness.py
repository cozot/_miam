# import
# ------------------------------------------------------------------------------------------
import os, sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import multiprocessing as mp

import easygui
import imageio

# miam import
import miam.image.Image as MIMG
import miam.image.imageType as MTYPE
import miam.image.channel as MCHN

import miam.processing.ColorSpaceTransform as MCST
import miam.processing.TMO_Lightness as TMO_L
import miam.processing.ContrastControl as PCC
import miam.histogram.Histogram  as MHIST
import miam.aesthetics.LightnessAesthetics as MLAC
import miam.aesthetics.Palette as MPAL
import miam.imageDB.ImageDB
import miam.imageDB.POGChecker
import miam.imageDB.HwHDRBuilder
import miam.html.generator
import miam.utils
import miam.pointcloud.PointCloud2D

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

def pCompLightnessMoments(filename):
    
    # cut filename
    path, name, ext = miam.utils.splitFileName(filename)

    # read image
    img = MIMG.Image.read(filename,exif=False)
    # compute L lightness
    L = img.getChannelVector(MCHN.channel.L)
    # get bins
    L_mean = np.mean(L)
    L_std  = np.std(L)
    L_skew = scipy.stats.skew(L)

    #print("<",name,">",end='')
    print("█",end='')
    sys.stdout.flush()

    return np.asarray([L_mean,L_std,L_skew])

def pCompHistogram(filename):
    
    # cut filename
    path, name, ext = miam.utils.splitFileName(filename)

    # read image
    img = MIMG.Image.read(filename,exif=False)
    # compute histogram
    histo = MHIST.Histogram.build(img,MIMG.channel.channel.L,50).normalise(norm='dot')
    # get bins
    bins = histo.histValue

    #print("<",name,">",end='')
    print("█",end='')
    sys.stdout.flush()


    return bins

if __name__ == '__main__':
    print("MIAM[POG Cluster Ligthness]")

    # ------------------------------------------------------------------------------------------
    # what to do !
    # ------------------------------------------------------------------------------------------
    # config: configfile
    jsonfile = "../DB/config_POG_DB.json"
    # all lightness moments file
    allLightness_Moments = "../DB/POG_DB_L_Moments.npy"

    # all lightness histogram file
    allLightness_50bins = "../DB/POG_DB_L_Hists_50bins.npy"
    allLightness_100bins = "../DB/POG_DB_L_Hists_100bins.npy"

    # scatter plot: sdt(mean)
    display_StdMean = True
    # ------------------------------------------------------------------------------------------

    # pog
    pogDB = miam.imageDB.ImageDB.ImageDB(jsonConfigFile ="../DB/config_POG_DB.json")
    pogDB.check(miam.imageDB.POGChecker.POGChecker(local=True))

    print("MIAM[",pogDB.name,":(csvFileName:: ",pogDB.csvFileName,", imagePATH::",pogDB.imagePATH,")]")

    # POG: compute Lightness Moments 
    # ------------------------------------------------------------------------------------------
    allMoments = None
    if not os.path.isfile(allLightness_Moments):
        print("MIAM[",pogDB.name," lightness moments not found !]")

        nbCpu = mp.cpu_count()
        print("MIAM[", nbCpu, "cpu cores]")
        print("MIAM[",pogDB.name,"  number of images:",len(pogDB.db),"]")
        print("MIAM[",pogDB.name," launch parallel computation of moments !]")

        _pool = mp.Pool()
        result = _pool.map(pCompLightnessMoments, pogDB.db)
        allMoments = np.asarray(result)
        print("")
        print("[ --------------------------------------------------------]")
        print("MIAM[",pogDB.name," parallel computation of moments done !]")
        print("MIAM[",pogDB.name," all moments:",allMoments.shape," ]")
        np.save("../DB/"+pogDB.name+'_L_Moments',allMoments)
        print("MIAM[",pogDB.name," all moments saved !]")
    else:
        print("")
        print("[ --------------------------------------------------------]")
        print("MIAM[",pogDB.name," lightness moments found !]")
        print("MIAM[",pogDB.name," all moments loaded !]")

        allMoments = np.load(allLightness_Moments)
        print("MIAM[",pogDB.name," all moments:",allMoments.shape," ]")

    # POG: display Lightness Moments
    # ------------------------------------------------------------------------------------------
    if display_StdMean:
        plt.figure("Lightness: standard deviations/mean")
        plt.plot(allMoments[:,0],allMoments[:,1],'bo',markersize=2)
        # k,r = miam.pointcloud.PointCloud2D.PointCloud2D(allMoments[:,0],allMoments[:,1]).removeIsolatedPoint(2.0,1)
        #k,r = miam.pointcloud.PointCloud2D.PointCloud2D(allMoments[:,0],allMoments[:,1]).removeIsolatedPoint(10.0,500)
        #plt.plot(k.X,k.Y,'bo',markersize=2)
        #plt.plot(r.X,r.Y,'ro',markersize=2)
        plt.plot(allMoments[:,0],allMoments[:,1],'bo',markersize=2)

        plt.show(block=True)

    # POG: compute Lightness Histograms
    # ------------------------------------------------------------------------------------------
    if not os.path.isfile(allLightness_50bins):
        print("MIAM[",pogDB.name," lightness histogram not found !]")

        nbCpu = mp.cpu_count()

        print("MIAM[", nbCpu, "cpu cores]")
        print("MIAM[",pogDB.name,"  number of images:",len(pogDB.db),"]")
        print("MIAM[",pogDB.name," launch parallel computation of histograms !]")

        _pool = mp.Pool()
        result = _pool.map(pCompHistogram, pogDB.db)
        allHist = np.asarray(result)
        print("")
        print("[ ---------------------------------------------------------- ]")
        print("MIAM[",pogDB.name," parallel computation of histograms done !]")
        print("MIAM[",pogDB.name," all histograms:",allHist.shape," ]")
        np.save("../DB/"+pogDB.name+'_L_Hists_50bins',allHist)
        print("MIAM[",pogDB.name," all histograms saved !]")
    else:
        print("")
        print("[ --------------------------------------------------------]")
        print("MIAM[",pogDB.name," lightness histogram found !]")


