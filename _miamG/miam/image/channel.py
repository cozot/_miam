# import
# ------------------------------------------------------------------------------------------
import enum
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class channel(enum.Enum):
    """ easy definition of channel """
    
    sR      = 0
    sG      = 1
    sB      = 2

    sRGB    = 3

    X       = 4
    Y       = 5
    Z       = 6

    XYZ     = 7

    L       = 8
    a       = 9
    b       = 10

    Lab     = 11

    def colorSpace(self):
        csIdx = self.value // 4
        res = None
        if csIdx   == 0:    res ='sRGB'
        elif csIdx == 1:    res = 'XYZ'
        elif csIdx == 2:    res = 'Lab'

        return res

    def getValue(self): return self.value % 4

    def toChannel(s):
        if s=='sR' :    return channel.sR
        elif s=='sG' :  return channel.sG
        elif s=='sB' :  return channel.sB

        elif s=='X' :   return channel.X
        elif s=='Y' :   return channel.Y
        elif s=='Z' :   return channel.Z

        elif s=='L' :   return channel.L
        elif s=='a' :   return channel.a
        elif s=='b' :   return channel.b

        else:           return channel.L

