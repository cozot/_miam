# import
# ------------------------------------------------------------------------------------------
import os, colour
# local
from . import Exif
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class ColorSpace(object):
    """mapping to colour.models.RGB_COLOURSPACES"""

    def buildFromExifData(dictExif, defaultCS = 'sRGB'):
        """ build colour.models from exif dict (dict) """

        # default color space
        colourspace= None

        exifColorSpace = ['Color Space', 'Profile Description']

        # first 'Color Space'
        if exifColorSpace[0] in dictExif:
            value = dictExif[exifColorSpace[0]]
            if 'sRGB' in value : 
                colourspace= colour.models.RGB_COLOURSPACES['sRGB'].copy()
            elif 'Adobe' in value : 
                colourspace= colour.models.RGB_COLOURSPACES['Adobe RGB (1998)'].copy()

        # no color space found, try profile description
        if not colourspace:
            if exifColorSpace[1] in dictExif:
                value = dictExif[exifColorSpace[1]]
                if 'sRGB' in value : 
                    colourspace= colour.models.RGB_COLOURSPACES['sRGB'].copy()
                elif 'Adobe' in value : 
                    colourspace= colour.models.RGB_COLOURSPACES['Adobe RGB (1998)'].copy()

        # no color space found, use default
        if not colourspace:
            # warning
            # print("WARNING[miam.image.ColorSpace.buildFromExif(",dictExif['File Name'],"):", "no colour space found in Exif Metadata >> ", defaultCS, " used by default]")
            colourspace = colour.models.RGB_COLOURSPACES[defaultCS].copy()

        return colourspace

    def buildFromExif(e, defaultCS = 'sRGB'): 
        """ build colour.models from Exif (object) """
        return ColorSpace.buildFromExifData(e.exif, defaultCS)

    def buildLab():
        return colour.RGB_Colourspace('Lab', primaries = None, whitepoint = None, whitepoint_name=None, 
                                      RGB_to_XYZ_matrix=None, XYZ_to_RGB_matrix=None, 
                                      cctf_encoding=None, cctf_decoding=None,
                                      use_derived_RGB_to_XYZ_matrix=False, use_derived_XYZ_to_RGB_matrix=False)

    def buildsRGB(): return colour.models.RGB_COLOURSPACES['sRGB'].copy()

    def buildXYZ():
        return colour.RGB_Colourspace('XYZ', primaries = None, whitepoint = None, whitepoint_name=None, 
                                      RGB_to_XYZ_matrix=None, XYZ_to_RGB_matrix=None, 
                                      cctf_encoding=None, cctf_decoding=None,
                                      use_derived_RGB_to_XYZ_matrix=False, use_derived_XYZ_to_RGB_matrix=False)
    def build(name='sRGB'):
        cs  = None
        if name== 'sRGB': cs =  colour.models.RGB_COLOURSPACES['sRGB'].copy()
        if name== 'Lab' : cs =  buildLab()
        if name== 'XYZ' : cs =  buildXYZ()
        return cs


