# import
# ------------------------------------------------------------------------------------------
import subprocess, os, sys
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class Exif(object):
    """store exif data read image file with exiftool"""

    def __init__(self):
        """ constructor """
        # attributes
        self.imageName = None               # image file name   (String)
        self.exif = None                    # exif data         (dict)

    def buildFromFileName(filename):
        """ return Exif (object) from image file """
        res = Exif()
        exifDict = dict()

        # reading metadata
        if os.path.exists(filename):
            try:
                exifdata = subprocess.check_output(['exiftool.exe','-a',filename],
                                            shell=True,
                                            universal_newlines=True,
                                            stdin=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
                exifdata = exifdata.splitlines()
            except:
                print("ERROR[miam.iamge.Exif.buildFromFileName(",filename,"): can not be read!]")
                exifdata =[]
            # buid exif dict
            for each in exifdata:
                # tags and values are separated by a colon
                tag,val = each.split(':',1) # '1' only allows one split
                exifDict[tag.strip()] = val.strip()

            # set attributes
            res.imageName = filename
            res.exif = exifDict
        else:
            print("ERROR[miam.iamge.Exif.buildFromFileName(",filename,"): file not found]")
            sys.exit()

        return res

    def __repr__(self):
        resKeys = ""
        for k in self.exif:
            resKeys = resKeys+"        "+k+": "+self.exif[k]+"\n"
        res =   "Exif{ \n " + \
                "   .imageName:" + self.imageName +"\n" + \
                "    .exifDict:{ \n" + \
                resKeys + "}}"
        return res

    def __str__(self): return self.__repr__()

