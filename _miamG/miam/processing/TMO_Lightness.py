# import
# ------------------------------------------------------------------------------------------
from . import Processing, Ymap
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class TMO_Lightness(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        Lightness tone mapping operator
        @params:
            img         - Required  : hdr image (miam.image.Image)
            kwargs      - Optionnal : optionnal parameter (dict)
                'removeZeros'       : true|false (boolean)
                'zerosPercentile'   : pertencile of min/max values removed (float)
        """
        # take into account optionnal parameters given as dict

        removeZeros = False
        zerosPercentile = None
        if kwargs:
            if 'removeZeros' in kwargs:
                removeZeros =  kwargs['removeZeros']
                if removeZeros:
                    if 'zerosPercentile' in kwags:
                        zerosPercentile = kwargs['zerosPercentile']
                    else:
                        print("WARNING[miam.processing.TMO_Lightness.compute(...): 'removeZeros'=True BUT no 'zerosPercentile' found! >> set to 0.0]")
                        zerosPercentile = 0.0

        # result: first copy image
        res = copy.deepcopy(img)

        # can tone map HDR only 
        if (img.type == image.imageType.imageType.HDR):

            # image preprocessing
            if removeZeros:
                if zerosPercentile==0:
                    res =  res.removeZeros()
                else:   
                    res =  res.removeZeros().removeZeros(zerosPercentile)

            # Y cnannel 
            Ychannel = ColorSpaceTransform.ColorSpaceTransform().compute(res,dest='XYZ').colorData[:,:,1]

            # min max 
            minY,maxY = np.amin(Ychannel), np.amax(Ychannel)
            
            # use Log(Y) as L
            Lchannel = 100*(np.log10(Ychannel)-np.log10(minY))/(np.log10(maxY)-np.log10(minY))

            # go back to Y
            Lab = np.zeros(res.shape)
            Lab[:,:,0] = Lchannel
            sdrYchannel = colour.Lab_to_XYZ(Lab, illuminant=np.array([ 0.3127, 0.329 ]))[:,:,1]

            # remap Y
            res = res.process(Ymap.Ymap(),oldY=Ychannel,newY=sdrYchannel)

            # equalize
            #colorDataEQ = skimage.exposure.equalize_hist(res.colorData)

            # merge
            #alpha =0.50
            #res.colorData = alpha*res.colorData + (1-alpha)*colorDataEQ
            # encode
            imgRGBprime = colour.cctf_encoding(res.colorData,function='sRGB')

            # update attributes
            res.colorData       = imgRGBprime
            res.type            = image.imageType.imageType.SDR
            res.linear          = False
            res.scalingFactor   = 1.0
            res.colorSpace      = colour.models.RGB_COLOURSPACES['sRGB'].copy()

        # reurn results
        return res.clip()

