# import
# ------------------------------------------------------------------------------------------
from . import Processing, LaplaceFilter, GaussianFilter
from .. import image
import colour, copy 
from scipy import ndimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class SumSquaredLaplace(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
        compute method:
        @params:
            self    - Required: (MIAM.processing.LaplaceFilter)
            img:    - Required: image on which Laplace is computed (MIAM.image.IMAGE)
            kwargs  - Optionnal: optionnal parameter (dict)
                 TO DO
   """
        # taking into account additional parameters
        if not kwargs: kwargs = {'nbZone': 9, 'preGaussian': True, 'postScaling': True}  # default value

        nbZone, preGaussian, postScaling = kwargs['nbZone'], kwargs['nbZone'], kwargs['postScaling']
    
        # pre-processing: gaussian filter
        if preGaussian:
            sigmaValue = max(img.colorData.shape[0],img.colorData.shape[1])/nbZone**2
            img = img.process(GaussianFilter.GaussianFilter(),sigma=sigmaValue)
                    
        # laplace filter
        img =  img.process(LaplaceFilter.LaplaceFilter())
        # squared laplace
        img = img**2
    
        # sum of channels WARNING: miam.image.Image to numpy
        sumLaplace2 = np.zeros(img.colorData.shape[0:2])
        sumLaplace2 = img.colorData[:,:,0]+img.colorData[:,:,1]+img.colorData[:,:,2]
        sumLaplace2 = sumLaplace2 / np.max(sumLaplace2)
    
        # zone and sum
        af = np.zeros((nbZone,nbZone))          # autofocus map
        stepW = sumLaplace2.shape[1]/nbZone
        stepH = sumLaplace2.shape[0]/nbZone
        for i in range(nbZone):
            for j in range(nbZone):
                af[j,i] = np.sum(sumLaplace2[(int(j*stepH)):(int((j+1)*stepH)-1),(int(i*stepW)):(int((i+1)*stepW)-1)])

        # post processing: scaling to One 
        if postScaling: 
            af = af/np.max(af)
        else: #average per pixel
            nbPixelPerZone = img.colorData.shape[0]*img.colorData.shape[1]
            af = af/nbPixelPerZone
     
        return image.Image.Image(af,
                                 "SumSquaredLaplace",
                                 type = image.imageType.imageType.SDR,                  # dot the best idea !
                                 linear = True,                                         # dot the best idea !
                                 colorspace = image.ColorSpace.ColorSpace.buildsRGB(),  # dot the best idea !
                                 scalingFactor = 1)

