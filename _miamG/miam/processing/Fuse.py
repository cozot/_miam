# import
# ------------------------------------------------------------------------------------------
from . import Processing
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class Fuse(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
            Add([img], {})
                !! assert !! len([img]) = 2 
                img[0] + img[1]

        """
        # kwargs

        if isinstance(img,list) :
            res = copy.deepcopy(img[0])

            if len(img) > 1: #  at least two images in the list
                for image in img[1:]:
                    res= res+image

        else: # not list
            res = copy.deepcopy(img)

        return res