# import
# ------------------------------------------------------------------------------------------
from . import Processing
import colour, copy, os
import miam.image.Image as MIMG
import miam.image.ColorSpace as MICS
import miam.image.imageType
import miam.histogram.Histogram as MHIST
import miam.aesthetics.LightnessAesthetics as MAE
import matplotlib.pyplot as plt
import numpy as np
# could be remove after check
# import skimage.color

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class ColorSpaceTransform(object):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 

        # first create a copy
        res = copy.deepcopy(img)

        if not kwargs:
            # " print WARNING message"
             print("WARNING[miam.processing.ColorSpaceTransform(",img.name,"):", "no destination colour space >> return a copy of image]")
            
        else:
            if not 'dest'in kwargs:
                print("WARNING[miam.processing.ColorSpaceTransform(",img.name,"):", "no 'dest' colour space >> return a copy of image]")

            else: # -> 'colorSpace' found
                if  kwargs['dest'] == 'Lab':  
                    # ---------------------------------------------------------------------------------------------------------------
                    # DEST: Lab
                    # ----------------------------------------------------------------------------------------------------------------
                    currentCS = img.colorSpace.name
                    # sRGB to Lab
                    if currentCS=="sRGB":                                                          
                        # ---------------------------------------------------------------------------------------------------------------
                        # sRGB to Lab
                        # ---------------------------------------------------------------------------------------------------------------
                        if not img.linear: apply_cctf_decoding=True 
                        else: apply_cctf_decoding=False
                        
                        # DEBUG
                        #print("DEBUG[sRGB > Lab]:apply_cctf_decoding>>",apply_cctf_decoding)

                        # sRGB to XYZ
                        RGB = res.colorData
                        XYZ = colour.sRGB_to_XYZ(RGB, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_method='CAT02', apply_cctf_decoding=apply_cctf_decoding)           
                        # XYZ to Lab
                        Lab = colour.XYZ_to_Lab(XYZ, illuminant=np.array([ 0.3127, 0.329 ]))

                        # update res
                        res.colorData = Lab
                        res.linear = None
                        res.colorSpace = MICS.ColorSpace.buildLab()

                    # XYZ to Lab
                    elif currentCS=="XYZ":                                                            # XYZ -> Lab
                        # ---------------------------------------------------------------------------------------------------------------
                        # XYZ to Lab
                        # ---------------------------------------------------------------------------------------------------------------
                           
                        XYZ = res.colorData
                        # XYZ to Lab
                        Lab = colour.XYZ_to_Lab(XYZ, illuminant=np.array([ 0.3127, 0.329 ]))

                        # update res
                        res.colorData = Lab
                        res.linear = None
                        res.colorSpace = MICS.ColorSpace.buildXYZ()
                    
                    # Lab to Lab
                    elif currentCS == "Lab":
                        # ---------------------------------------------------------------------------------------------------------------
                        # Lab to Lab
                        # ---------------------------------------------------------------------------------------------------------------
                        # return a copy
                        pass

                elif kwargs['dest'] == 'sRGB':
                    # ---------------------------------------------------------------------------------------------------------------
                    # DEST: sRGB
                    # ----------------------------------------------------------------------------------------------------------------
                    currentCS = img.colorSpace.name

                    # Lab to sRGB
                    if currentCS=="Lab":                                         
                        # ---------------------------------------------------------------------------------------------------------------
                        # Lab to sRGB
                        # ---------------------------------------------------------------------------------------------------------------                        
                        Lab = res.colorData
                        # Lab to XYZ
                        XYZ = colour.Lab_to_XYZ(Lab, illuminant=np.array([ 0.3127, 0.329 ]))

                        # XYZ to sRGB
                        if img.type == MIMG.imageType.imageType.HDR : apply_cctf_encoding = False
                        else : apply_cctf_encoding = True

                        sRGB = colour.colour.XYZ_to_sRGB(XYZ, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_transform='CAT02', apply_cctf_encoding=apply_cctf_encoding)

                        # update res
                        res.colorData = sRGB
                        res.colorSpace = MICS.ColorSpace.buildsRGB()
                        res.linear = not apply_cctf_encoding
                    
                    # XYZ to sRGB
                    elif currentCS == "XYZ":
                        # ---------------------------------------------------------------------------------------------------------------
                        # XYZ to sRGB
                        # --------------------------------------------------------------------------------------------------------------- 
                        XYZ = res.colorData
                        # XYZ to sRGB
                        if img.type == MIMG.imageType.imageType.HDR : apply_cctf_encoding = False
                        else: apply_cctf_encoding = True
                        sRGB = colour.colour.XYZ_to_sRGB(XYZ, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_transform='CAT02', apply_cctf_encoding=apply_cctf_encoding)

                        # update res
                        res.colorData = sRGB
                        res.colorSpace = MICS.ColorSpace.buildsRGB()
                        res.linear = not apply_cctf_encoding
    
                        # sRGB to sRGB
                    elif currentCS == "sRGB":
                        # return a copy
                        pass              
                    else:
                        print("WARNING[miam.processing.ColorSpaceTransform(",img.name,"):", "'dest' colour space:",kwargs['dest'] , "not yet implemented !]")

                elif kwargs['dest'] == 'XYZ':
                    # ---------------------------------------------------------------------------------------------------------------
                    # DEST: XYZ
                    # ----------------------------------------------------------------------------------------------------------------
                    currentCS = img.colorSpace.name
                    # sRGB to XYZ
                    if currentCS=="sRGB":                                                          
                        # ---------------------------------------------------------------------------------------------------------------
                        # sRGB to XYZ
                        # ---------------------------------------------------------------------------------------------------------------
                        if  img.type == MIMG.imageType.imageType.SDR : 
                            apply_cctf_decoding=True 
                        else: 
                            apply_cctf_decoding=False
                        
                        # DEBUG
                        #print("DEBUG[sRGB > XYZ]:apply_cctf_decoding>>",apply_cctf_decoding)
                        
                        # sRGB to XYZ
                        RGB = res.colorData
                        XYZ = colour.sRGB_to_XYZ(RGB, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_method='CAT02', apply_cctf_decoding=apply_cctf_decoding)           

                        # update res
                        res.colorData = XYZ
                        res.linear = True
                        res.colorSpace = MICS.ColorSpace.buildXYZ()

                    # Lab to XYZ
                    elif currentCS=="XYZ":                                                         
                        # ---------------------------------------------------------------------------------------------------------------
                        # XYZ to XYZ
                        # ---------------------------------------------------------------------------------------------------------------
                         # return a copy
                         pass
                    
                    # Lab to XYZ
                    elif currentCS == "Lab":
                        # ---------------------------------------------------------------------------------------------------------------
                        # Lab to XYZ
                        # ---------------------------------------------------------------------------------------------------------------
                        Lab = res.colorData
                        # Lab to XYZ
                        XYZ = colour.Lab_to_XYZ(Lab, illuminant=np.array([ 0.3127, 0.329 ]))

                        # update res
                        res.colorData = XYZ
                        res.linear = True
                        res.colorSpace = MICS.ColorSpace.buildXYZ()

        return res

def XYZ_to_sRGB(XYZ, apply_cctf_encoding=True):
    RGB =  colour.XYZ_to_sRGB(XYZ, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_transform='CAT02', apply_cctf_encoding=apply_cctf_encoding)
    return RGB

def sRGB_to_XYZ(RGB, apply_cctf_decoding=True):
    XYZ = colour.sRGB_to_XYZ(RGB, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_method='CAT02', apply_cctf_decoding=apply_cctf_decoding)           
    return XYZ

def Lab_to_XYZ(Lab, apply_cctf_encoding=True):
    XYZ = colour.Lab_to_XYZ(Lab, illuminant=np.array([ 0.3127, 0.329 ]))
    return XYZ

def XYZ_to_Lab(XYZ, apply_cctf_decoding=True):
    Lab = colour.XYZ_to_Lab(XYZ, illuminant=np.array([ 0.3127, 0.329 ]))
    return Lab

def Lab_to_sRGB(Lab, apply_cctf_encoding=True):
    XYZ = colour.Lab_to_XYZ(Lab, illuminant=np.array([ 0.3127, 0.329 ]))
    RGB =  colour.XYZ_to_sRGB(XYZ, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_transform='CAT02', apply_cctf_encoding=apply_cctf_encoding)
    return RGB

def sRGB_to_Lab(RGB, apply_cctf_decoding=True):
    XYZ = colour.sRGB_to_XYZ(RGB, illuminant=np.array([ 0.3127, 0.329 ]), chromatic_adaptation_method='CAT02', apply_cctf_decoding=apply_cctf_decoding)           
    Lab = colour.XYZ_to_Lab(XYZ, illuminant=np.array([ 0.3127, 0.329 ]))
    return Lab

