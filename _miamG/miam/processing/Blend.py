# import
# ------------------------------------------------------------------------------------------
from . import Processing
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class Blend(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
            Blend([img], {'alpha':alpha})
                len([img]) = 2 / alpha in [0,1]
                img[0]*alpha + img[1]*(1 - alpha)

        """
        # kwargs
        alpha = 0.5
        if kwargs and ('alpha' in kwargs) :
            alpha = kwargs['alpha']

        if isinstance(img,list) :
            if len(img) == 2: # two images in the list
                img0, img1 = img[0], img[1]
                res = img0*alpha + img1*(1-alpha)

                # HDR or SDR
                if img0.isHDR() or img1.isHDR():
                    res.type = image.imageType.imageType.HDR

            else: # not  two images in the list
                return copy.deepcopy(img[0])
        else: # not list
            res = copy.deepcopy(img)

        return res
