# import
# ------------------------------------------------------------------------------------------
from . import Processing, Ymap
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage, math
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class ExposureControl(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        Linear tone mapping operator
        @params:
            img         - Required  : hdr image (miam.image.Image)
            kwargs      - Optionnal : optionnal parameter (dict)
                'min'               : minimum value under which output is zero (float)
                'max'               : maximum value above which output is one (float)
        """
 
        minValue = 0.0
        maxValue = 1.0

        if not kwargs: kwargs = {'auto': True, 'target': 0.5, 'EV': 0}  # default value

        # auto
        if 'auto' in kwargs: 
            # auto or manual mode
            auto = kwargs['auto']
        else:
            # if not auto in kwargs -> manual mode
            auto = False

        # target or EV value
        if auto:
            # auto: target value
            if 'target' in kwargs:
                target = kwargs['target']
            else:
                target = 0.5
        
        # EV value
        if 'EV' in kwargs:
            EV = kwargs['EV']
        else:
            EV = 0
        
        res = copy.deepcopy(img)
        rgbIn = res.colorData

        if auto: 
            # compute mean Y (Luminance)
            y = res.getChannel(image.channel.channel.Y)
            if res.hasMask():
                if res.binaryMask(): 
                    # DEBUG
                    print("ExposureControl.compute(): BINARY mask taken into account")
                    ymean = np.mean(y[res.mask==1])
                else:
                    # DEBUG
                    print("ExposureControl.compute(): NON BINARY mask taken into account")
                    ymean = np.mean(y*res.mask[...,np.newaxis])
            else: # no mask
                ymean = np.mean(y)
            rgbOut = rgbIn*(target/ymean)*math.pow(2,EV)
        else: 
            rgbOut = rgbIn*math.pow(2,EV)


        # clip if SDR 
        if (img.type == image.imageType.imageType.SDR):
            rgbOut[rgbOut>=1.0] = 1.0
            rgbOut[rgbOut<=0.0] = 0.0

        # update attributes
        res.colorData       = rgbOut

        return res

