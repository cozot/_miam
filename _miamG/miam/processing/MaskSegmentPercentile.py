# import
# ------------------------------------------------------------------------------------------
from . import Processing
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage, functools
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class MaskSegmentPercentile(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
            MaskSegmentPercentile(img, {'percent':[percentileValue], 'channel':'Y'})
        """
        # kwargs
        # percentile values
        percentileValues = [0,50,100]   # default value
        if kwargs and ('percent' in kwargs) :  percentileValues = kwargs['percent']
        # percentile values
        channel = 'Y' if img.isHDR() else 'L'   # default value
        if kwargs and ('channel' in kwargs) : channel = kwargs['channel']

        res = []

        for pValmin,pValmax in zip(percentileValues, percentileValues[1:]):
            seg = copy.deepcopy(img)
            seg.addMask(one=False)
            # getChannel
            c = seg.getChannel(image.channel.channel.toChannel(channel))
            pMin, pMax = np.percentile(c, (pValmin, pValmax))
            seg.mask[(c>= pMin) & (c<=pMax) ] = 1
            res.append(seg)
        return res


