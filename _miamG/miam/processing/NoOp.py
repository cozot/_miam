# import
# ------------------------------------------------------------------------------------------
from . import Processing
import copy
# ------------------------------------------------------------------------------------------
class NoOp(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        Duplicate image
        """

        res = copy.deepcopy(img)

        return res


