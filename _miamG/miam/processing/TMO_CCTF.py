# import
# ------------------------------------------------------------------------------------------
from . import Processing
from .. import image
import colour, copy
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class TMO_CCTF(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        CCTF tone mapping operator
        @params:
            img         - Required  : hdr image (miam.image.Image)
            kwargs      - Optionnal : optionnal parameter (dict)
                'function'          : 'sRGB' (string)
        """

        if not kwargs:
            function = 'sRGB'
        elif 'function' in kwargs:
            function = kwargs['function']
        else:
            function = 'sRGB'
 
        res = copy.deepcopy(img)

        # can tone map HDR only 
        if (img.type == image.imageType.imageType.HDR):

            # encode
            imgRGBprime = colour.cctf_encoding(res.colorData,function=function)

            # update attributes
            res.colorData       = imgRGBprime
            res.type            = image.imageType.imageType.SDR
            res.linear          = False
            res.scalingFactor   = 1.0
            res.colorSpace      = colour.models.RGB_COLOURSPACES[function].copy()

        return res.clip()

