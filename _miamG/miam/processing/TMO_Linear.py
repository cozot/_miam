# import
# ------------------------------------------------------------------------------------------
from . import Processing
from .. import image
import colour, copy
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class TMO_Linear(Processing.Processing):
    """description of class"""
    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        Linear tone mapping operator
        @params:
            img         - Required  : hdr image (miam.image.Image)
            kwargs      - Optionnal : optionnal parameter (dict)
                'min'               : minimum value under which output is zero (float)
                'max'               : maximum value above which output is one (float)
        """
 
        minValue = 0.0
        maxValue = 1.0

        if not kwargs:
            kwargs = {'min': 0.0, 'max': 1.0}

        # min value
        if 'min' in kwargs:
            minValue = kwargs['min']
        else:
            minValue = 0.0

        # max value
        if 'max' in kwargs:
            maxValue = kwargs['max']
        else:
            maxValue = 1.0
 
        res = copy.deepcopy(img)

        # can tone map HDR only 
        if (img.type == image.imageType.imageType.HDR):

            # value between [min,max] -> [0,1]
            imgRGB = (res.colorData-minValue)/(maxValue-minValue)

            imgRGB[imgRGB>=1.0] = 1.0
            imgRGB[imgRGB<=0.0] = 0.0

            # encode
            imgRGBprime = colour.cctf_encoding(imgRGB,function='sRGB')

            # update attributes
            res.colorData       = imgRGBprime
            res.type            = image.imageType.imageType.SDR
            res.linear          = False
            res.scalingFactor   = 1.0   # res.scalingFactor/(max-min)

        return res

