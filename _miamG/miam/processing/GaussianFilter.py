# import
# ------------------------------------------------------------------------------------------
from . import Processing
from .. import image
import colour, copy 
from scipy import ndimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class GaussianFilter(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        compute method:
            @params:
            self    - Required: (MIAM.processing.LaplaceFilter)
            img:    - Required: image on which Laplace is computed (MIAM.image.IMAGE)
            kwargs  - Optionnal: optionnal parameter (dict)
                 'sigma'= scalar or sequence of scalars
        """

        res = copy.deepcopy(img)

        # taking into account optional parameters
        if not kwargs: kwargs = {'sigma': 10}  # default value

        sigma = kwargs['sigma']

        # compute lapalce filter
        res.colorData = imgGaussian=ndimage.gaussian_filter(res.colorData,sigma, mode='reflect', cval=0.0)


        return res



