# import
# ------------------------------------------------------------------------------------------
from . import Processing
from . import ColorSpaceTransform
from .. import image
import colour, copy, skimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class Ymap(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        res = copy.deepcopy(img)
        if kwargs and ('oldY' in kwargs) and ('newY' in kwargs):
            Yold = kwargs['oldY']
            Ynew = kwargs['newY']

            res.colorData[:,:,0] = res.colorData[:,:,0]*Ynew/Yold
            res.colorData[:,:,1] = res.colorData[:,:,1]*Ynew/Yold
            res.colorData[:,:,2] = res.colorData[:,:,2]*Ynew/Yold

        return res