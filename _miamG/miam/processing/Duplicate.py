# import
# ------------------------------------------------------------------------------------------
from . import Processing
import copy
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class Duplicate(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """
        Duplicate image
        """
 
        nbDuplicate = 1

        if not kwargs: kwargs = {'nb': 1}

        # number of duplicate
        if 'nb' in kwargs: 
            # auto or manual mode
            nbDuplicate = kwargs['nb']
        else:
            # if not auto in kwargs -> manual mode
            nbDuplicate = 1

        res = [] 
        for i in range(nbDuplicate):
            res.append(copy.deepcopy(img))

        return res



