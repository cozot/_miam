# import
# ------------------------------------------------------------------------------------------
from . import Processing
from .. import image
import  copy 
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class ToOne(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
        compute method:
        @params:
            self    - Required: (MIAM.processing.LaplaceFilter)
            img:    - Required: image on which Laplace is computed (MIAM.image.IMAGE)
            kwargs  - Optionnal: optionnal parameter (dict)
                'method'= 'scalingMax' | scalingMinMax | 'crop'
                    if scalingMax img.colorData = img.colorData/max(img.colorData)
                    if scalingMinMax img.colorData = (img.colorData  - min(img.colorData))/(max(img.colorData) - min(img.colorData))
                    if crop img.colorData[img.colorData>1]=1 and img.colorData[img.colorData<0]=0

    """
        res = copy.deepcopy(img)

        if not kwargs: kwargs = {'method': 'scalingMax'}  # default value

        # taking into account optional parameters
        if kwargs['method'] == 'scalingMax':
            minIMG = np.min(res.colorData)
            res.colorData = res.colorData/maxIMG        
        elif kwargs['method'] == 'scalingMinMax':
            minIMG = np.min(res.colorData)
            maxIMG = np.max(res.colorData)
            res.colorData = (res.colorData-minIMG)/(maxIMG-minIMG)
        elif kwargs['method'] == 'crop':
            res.colorData[res.colorData>1] = 1
            res.colorData[res.colorData<0] = 0
        else:
            print("WARNING[miam.processing.ToOne: unknown 'method'=",kwargs['method'],"]")

        return res





