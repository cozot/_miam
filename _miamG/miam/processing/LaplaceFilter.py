# import
# ------------------------------------------------------------------------------------------
from . import Processing
from .. import image
import colour, copy 
from scipy import ndimage
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class LaplaceFilter(Processing.Processing):
    """description of class"""

    def __init__(self):
        pass

    def compute(self, img, **kwargs): 
        """ 
        compute method:
        @params:
            self    - Required: (MIAM.processing.LaplaceFilter)
            img:    - Required: image on which Laplace is computed (MIAM.image.IMAGE)
            kwargs  - Optionnal: optionnal parameter (dict)
    """
        res = copy.deepcopy(img)

        # taking into account optional parameters
        if kwargs:
            pass
        else:
            pass

        # compute lapalce filter
        res.colorData = ndimage.laplace(res.colorData, output=None, mode='reflect', cval=0.0)

        return res



