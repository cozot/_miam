import matplotlib.pyplot as plt
from . import KmeanDisplay

class histCosineKmeanDisplay(KmeanDisplay.KmeanDisplay):
	"""description of class"""
	
	def __init__(self, L_mean, L_std, pcolor= None):
		# map of image according to Lightness mean and std
		self.L_mean = L_mean
		self.L_std  = L_std
		# colors
		self.colors = ['b', 'g','r','c','m','y','k'] if pcolor == None else pcolor

		# figure and axes
		fig, ax = plt.subplots(1,2)
		fig.suptitle('Histogram Classification by k-means[cosine distance]')

		self.fig = fig
		self.axes = ax

	def plot(self, centroids, assigmentsIdx, iter):
		# display centroids shape
		self.axes[0].cla()

		for i,c in enumerate(centroids): self.axes[0].plot(c,self.colors[i%len(self.colors)])

		self.axes[0].set_title("centroids (iter:"+str(iter)+")")

		# display assigments
		if isinstance(L_mean, np.ndarray) and isinstance(L_std,np.ndarray):
			self.axes[1].cla()
			self.axes[1].set_title("assigments map")

			for i,assigmentId in enumerate(assigmentsIdx):
				self.axes[1].plot(L_mean[assigmentId],L_std[assigmentId],self.colors[i%len(self.colors)]+'o', markersize=5)

		plt.pause(0.05)