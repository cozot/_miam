from functools import partial

class generator(object):
    """description of class"""

    def tag(tagName, content, attributeName= None, attributeValue=None, close=True):
        # taking attribute into account
        if attributeName and attributeValue:
            if isinstance(attributeName, list) and isinstance(attributeValue, list):
                minLen = min(len(attributeName),len(attributeName))
                attrib =''
                for i in range(minLen):
                    attrib = attrib+attributeName[i]+'="'+attributeValue[i]+'" '
            else:
                attrib = attributeName+'="'+attributeValue+'"'
            bBalise= '<'+tagName+' '+attrib+'>'
        else:
            bBalise= '<'+tagName+'>'
        
        # taking close into account
        eBalise= '</'+tagName+'>' if close else ''

        # return
        return bBalise+content+eBalise

    imgTagWidth = partial(tag, tagName='img', content='', attributeName= ['src','width'], close=False)
    imgTag = partial(tag, tagName='img', content='', attributeName= 'src', close=False)
    bold = partial(tag, tagName='b',  attributeName= None, attributeValue=None, close=True)
    li = partial(tag, tagName='li',  attributeName= None, attributeValue=None, close=True)
    ul = partial(tag, tagName='ul',  attributeName= None, attributeValue=None, close=True)
    td = partial(tag, tagName='td',  attributeName= None, attributeValue=None, close=True)
    tr = partial(tag, tagName='tr',  attributeName= None, attributeValue=None, close=True)

    def listItem(nameItem_item):
        res = ''
        for it in nameItem_item:
            res+=generator.li(content=generator.bold(content=it[0]+':')+it[1])+'\n'
        return generator.ul(content='\n'+res)

    def table(tableElt):
        r = ''
        for row in tableElt:
            d = ''
            for elt in row: d+=generator.td(content=elt)
            r += generator.tr(content=d)+'\n'
        return generator.tag('table', '\n'+r)

# --------------------------------------------------------------------------------------
# test
# --------------------------------------------------------------------------------------
def test():
    print(generator.tag('H1', 'HDR DataBase'))
    print(generator.imgTag(attributeValue='../image/test.jpg'))
    print(generator.listItem([['toto','titi'],['momo','mimi'],['roro','riri']]))
    print(generator.table([['toto','titi'],['momo','mimi'],['roro','riri']]))





