import numpy as np
import alphashape
import shapely
#import shapely.geometry
import matplotlib.pyplot as plt

class PointCloud2D(object):
    """description of class"""
    
    # constructor
    # -----------------------------------------------------------------------------
    def __init__(self,x,y):
        
        # attibutes
        self.X = x
        self.Y = y
        
    # methods
    # -----------------------------------------------------------------------------     
    def removeIsolatedPoint(self, dist, nbPoint):

        keepX = []
        keepY = []

        removeX = []
        removeY = []

        for i in range(len(self.X)):

            x, y=self.X[i], self.Y[i]

            idx = (self.X >( x-dist/2)) & (self.X < (x+dist/2)) & (self.Y > (y-dist/2)) & (self.Y < (y+dist/2))
            if len(idx[idx==True])> nbPoint:
                keepX.append(x)
                keepY.append(y)
            else:
                removeX.append(x)
                removeY.append(y)

        return PointCloud2D(keepX,keepY),PointCloud2D(removeX,removeY)

    def toPoint(self):
        res = []
        for i, x in enumerate(self.X):
            res.append((x,self.Y[i]))
        return res
    
    def contour(self, alpha):
        return alphashape.alphashape(self.toPoint(), alpha)

    def convexHull(self):
        return shapely.geometry.MultiPoint(self.toPoint()).convex_hull
    
    def plot(self, mark):
        plt.plot(self.X,self.Y,mark)
    
    # class methods
    # -----------------------------------------------------------------------------
    def toXYarray(poly):
        
        X, Y = [], []
 
        listOfPoints = list(poly.exterior.coords)
    
        for xy in listOfPoints:
            x,y = xy
            X.append(x)
            Y.append(y)
        
        return X,Y






