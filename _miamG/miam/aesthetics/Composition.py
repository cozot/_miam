# import
# ------------------------------------------------------------------------------------------
import copy
import numpy as np
# local
import miam.utils
import miam.image.Image as MIMG
import miam.processing.SumSquaredLaplace as MSSL
import miam.pointcloud.PointCloud2D as MPC2D

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class Composition(object):
    """ 
    class Composition:
        attribute(s):
            name:       object name
            nbPoints:   number of point in composition
            points:     list points[(x,y)]*nbPoints
    """    
    # constructor
    def __init__(self, name, points,imgShape):
        self.name: name                         # name (str)
        self.points = points                    # list of tunple [(x,y)] * npPoints in [0..1] range
        self.nbPoints = len(self.points)        # number of Points
        self.shape = imgShape                # shape of image from which it is

    # build
    def build(image, nbPoint=10, nbZone=9, **kwargs):
        # taking into account additional parameters
        if not kwargs: kwargs = {'preGaussian': True, 'postScaling': True}  # default value
        preGaussian, postScaling = kwargs['preGaussian'], kwargs['postScaling']

        # compute Sum of Squared Laplace map
        sslMap = image.process(MSSL.SumSquaredLaplace(),
                               nbZone= nbZone, 
                               preGaussian = preGaussian, 
                               postScaling= postScaling)

        # extract maximum focus (ie max sum squared Laplacien)
        points = []
        sslArray = np.argsort(sslMap.colorData.flatten())
        for i in range(-1,-(nbPoint+1),-1):
            p = np.divmod(sslArray[i],nbZone)
            x,y = (p[1]+0.5)/nbZone,  (p[0]+0.5)/nbZone
            points.append((x,y))

        (path,name, zxt) = miam.utils.splitFileName(image.name)
        return Composition(name+"(cpmposition)", points, image.shape[0:2])

    # plot
    def plot(self,ax, marker = None,shortName=True, title=True, keepRawPoints = False):
        marker = 'go-' if not marker else marker
        # to image space
        W, H =self.shape[1], self.shape[0]
        pImageSpace = []
        for p in self.points:
                xIS, yIS = p
                xIS = xIS*W
                yIS = yIS*H
                pImageSpace.append((xIS,yIS))
        # to X, Y array
        X, Y, XRAW, YRAW = [],[], [], []
        for p in pImageSpace:
            x,y = p
            X.append(x)
            Y.append(y)
        XRAW = copy.deepcopy(X)
        YRAW = copy.deepcopy(Y)

        if self.nbPoints == 3: # display triangle
            X.append(X[0])
            Y.append(Y[0])
            keepRawPoints = False

        else: # display convex Hull
            pc = MPC2D.PointCloud2D(X,Y)
            X,Y = MPC2D.PointCloud2D.toXYarray(pc.convexHull())

        if keepRawPoints : ax.plot(XRAW,YRAW,'ro')
        ax.plot(X,Y,marker)
        ax.set_xlim(0,W)
        ax.set_ylim(H,0)
        name = self.name if not shortName else "(comp.)"
        if title: ax.set_title(name)

        ax.axis("on")


