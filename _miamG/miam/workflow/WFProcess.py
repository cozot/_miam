# import
# ------------------------------------------------------------------------------------------
import os, sys, math, functools
import multiprocessing as mp
import matplotlib
import numpy as np
import easygui
import colour

# import Qt


# QT matplotlib

# miam import
from . import WFNode

import miam.image.Image as MIMG
import miam.histogram.Histogram as MHIST
import miam.image.channel
import miam.utils

# gui import


# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class WFProcess(WFNode.WFNode):
    """description of class"""
    def __init__(self, name = 'processing:noname', process=None):
        super().__init__(name)

        # attibutes
        self.inputs = []
        self.process = process
        self.parameters ={}
        self.outputs = []

    def setParameters(self,p):
        self.parameters = p
        return self

    def isReady(self):
        res =  functools.reduce(
            lambda x,y : x and y,
            list(map(lambda x: x.ready, self.inputs)),
            True)
        print("WFProcess[",self.name," is ready for computation:", res,"]")
        return res

    def compute(self):
        # managing input
        # -----------------------------------------------------------------------------------------
        # if single input:      take self.inputs[0].image !! note that self.inputs[0].image could be a list
        # if multiple inputs:    create a list of each input
        # -----------------------------------------------------------------------------------------
        if len(self.inputs)== 1:
            # single input
            img = self.inputs[0].image
        else:
            # multiple inputs
            img =[]
            for input in self.inputs:
                img.append(input.image)

        # compute process object
        # -----------------------------------------------------------------------------------------
        # img is single Image or Image list
        # -----------------------------------------------------------------------------------------
        print('WFProcess[',self.name,'].compute(',self.parameters,')')
        resImg = self.process.compute(img,**self.parameters)

        # push results
        # -----------------------------------------------------------------------------------------
        # two cases:
        #   1 - len of resImg is be equal to len of outputs: push one image to output
        #   2 - len of resImg > 1 and len of output is equal to 1: push image list to single output
        # -----------------------------------------------------------------------------------------
        if not isinstance(resImg,list):
            if len(self.outputs) == 1 :
                # case 1: just a single Image, just a single ouput
                self.outputs[0].image = resImg
                self.outputs[0].ready = True
            else:
                # error
                print("[ERROR] WFProcess(WFNode.WFNode).compute:: resImg is single Image but multiple outputs!")
        else:
            # a list of Image
            if len(resImg) == len(self.outputs):
                # case 1
                for i in range(len(self.outputs)):
                    self.outputs[i].image = resImg[i]
                    self.outputs[i].ready = True

            else:
                # case 2
                self.outputs[0].image = resImg
                self.outputs[0].ready = True






