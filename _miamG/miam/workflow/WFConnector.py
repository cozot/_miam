# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import multiprocessing as mp
import matplotlib
import numpy as np
import easygui
import colour

# miam import
from . import WFNode

import miam.image.Image as MIMG
import miam.histogram.Histogram as MHIST
import miam.image.channel
import miam.utils

# gui import


# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class WFConnector(WFNode.WFNode):
    """description of class"""
    def __init__(self, name ='image:noname'):
        super().__init__(name)

        # attributes
        self.isRoot         = False
        self.isLeaf         = False

        self.outputOf       = None  # WFProcess
        self.inputOf        = None  # WFProcess

        self.image          = None  # single Image or list of Image

        self.ready          = False

