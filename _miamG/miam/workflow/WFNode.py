# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import multiprocessing as mp
import matplotlib
import numpy as np
import easygui
import colour

# miam import
import miam.image.Image as MIMG
import miam.histogram.Histogram as MHIST
import miam.image.channel
import miam.utils

# gui import


# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------
class WFNode(object):
    """description of class"""
    id = 0
    # constructor
    def __init__(self,name='node:noname'):
        self.id = WFNode.id
        WFNode.id += 1
        self.name = name
    pass

