import os, json
import numpy as np
from . import Builder, ImageDB, utils

class HwHDRBuilder(Builder.Builder):
    """description of class"""

    def __init__(self):
        super().__init__()

    def compute(self,db, src):
        super().compute(db,src)

        # recover data from config
        imagePATH = self.config["imagePATH"]
        cvsFilePATH = self.config["cvsFilePATH"]

        # get database compenents
        components = self.config['components']

        # build is done according to a source
        for c in components:
            if src in c: configSRC= c
        # source path
        srcPath = imagePATH + configSRC[src]['path']

        # get file in source path
        l = os.listdir(srcPath)

        # get only files with right name
        srcList = []
        for f in l:
           if utils.checkString(f,configSRC[src]) : srcList.append(f)

        # srcList contains src file with correct format
        # check other compoents
        requiredFiles = []

        # for all file in scrList (source file)
        for srcfile in srcList:
            # listPerSourceFile
            listPerSourceFile = []
            # get number
            n = utils.filename_to_number(srcfile,configSRC[src])
            listPerSourceFile.append(configSRC[src]['path']+srcfile)
            # for other components
            for comp in components:
                if not src in comp: # if not the source
                    currentConfig = comp
                    # get name of compenent
                    k = list(currentConfig.keys())[0]
                    # get config compoenent
                    kv = currentConfig[k]
                    listPerSourceFile.extend(utils.number_to_filenames(n,kv))

            requiredFiles.append(listPerSourceFile)

        # save as csv file
        np.savetxt( db.csvFileName, requiredFiles, delimiter=";",fmt="%s",encoding="utf8")

        # update db in db
        db.db = requiredFiles





