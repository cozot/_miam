import os, json
import numpy as np

class ImageDB(object):
    """Generic Class for image-database
    main assumption:
    DB: CSV files with image link (path or uri)
    CSV file structure: per row (uri; uri; uri; ...) or (filename; filename; filename; ...)
    """
    def __init__(self, jsonConfigFile = None):
        """
        json file format:
            {
	            "name": "my_DB",                            <- database name                                                       
	            "imagePATH": "../images/",                  <- where are local copy of images
	            "csvFilePATH": "../imagefile_utf8.csv",     <- image path or uri list in csv format
            }
        """

        # attribute
        self.jsonConfigFile = jsonConfigFile

        # load config
        with open(self.jsonConfigFile) as json_file: config = json.load(json_file)

        # recover cvsFileName from config file  
        self.name = config["name"]
        self.csvFileName = config["csvFilePATH"]
        self.imagePATH = config["imagePATH"]

        # DEBUG
        # print("DEBUG[ImageDB.name::",self.name,"]")
        # print("DEBUG[ImageDB.csvFileName::",self.csvFileName,"]")
        # print("DEBUG[ImageDB.imagePATH::",self.imagePATH,"]")

        # database set to None
        self.db = None

        if  os.path.isfile(self.csvFileName):
            # DEBUG
            # print("DEBUG[ImageDB.csvFileName::",self.csvFileName,"> load csv]")

            self.db = np.loadtxt(self.csvFileName, dtype=np.unicode_, delimiter=";",encoding="utf8")

            #DEBUG
            # print("DEBUG[ImageDB.db.shape::",self.db.shape,"]")
        pass

    def build(self,builder,src=""):
        """ build cvs file """
        builder.compute(self,src)

    def check(self,checker):
        """ check if images in cvs are in local storage """
        checker.compute(self)

    def lookForUpdate(self):
        pass

    def update(self):
        pass



