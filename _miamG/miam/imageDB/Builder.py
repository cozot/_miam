from . import ImageDB
import os, json
class Builder(object):
    """description of class"""

    def __init__(self):
        self.config = None

    def compute(self,db,src):


        # CSV file should NOT still exist
        if db.csvFileName:
            if  os.path.isfile(db.csvFileName): print("WARNING[miam.imageDB.Builder.compute(...):",db.csvFileName," already exists! It will be overwritten!]")

        # config file
        if db.jsonConfigFile:
            with open(db.jsonConfigFile) as json_file: 
                self.config = json.load(json_file)
        else:
            print("ERROR[miam.imageDB.Builder.compute(...):",db.jsonConfigFile," does not exist !]")
