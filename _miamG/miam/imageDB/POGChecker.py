from . import ImageDB, Checker, utils
import os, json
import numpy as np

class POGChecker(Checker.Checker):
    """description of class"""
    def __init__(self, local=True):
        """ 

            https://www.portraitsofgirls.com/wp-content/uploads/2020/02/alexandra-ola-by-maxim-gagarin-14.jpg
        """
        self.local = local  # if local local copy are checked else check online images

    def compute(self, db):
        if self.local:
            # check local copy
            # get path to local images
            path = db.imagePATH

            # results of checking
            checked = []
            missingFiles = []
            duplicateFiles= []

            # for all item in db
            for imageURI in db.db:
                # check if locol copy of image exist
                filename, ext =utils.img_url_2_file_ext(path,imageURI,minRange=-3,maxRange=-1) 
                filename = filename+'.'+ext
                imgOK = os.path.isfile(filename)

                if imgOK:
                    # check for duplicate
                    if not filename in checked:
                        checked.append(filename)
                    else:
                        duplicateFiles.append(filename)
                else:
                    missingFiles.append(filename)

            # results
            if len(duplicateFiles)>0: np.savetxt(db.name+"_"+"duplicateImages.csv",duplicateFiles,delimiter=";",fmt="%s",encoding="utf8")
            if len(missingFiles)>0:np.savetxt(db.name+"_"+"missingImages.csv",missingFiles,delimiter=";",fmt="%s",encoding="utf8")

            # DEBUG
            # print("DEBUG[POGChecker.compute(",db.name,")::missing images>",len(missingFiles),"]")
            # print("DEBUG[POGChecker.compute(",db.name,")::duplicate images>",len(duplicateFiles),"]")
            # print("DEBUG[POGChecker.compute(",db.name,")::",len(db.db)," uri tested ->",len(checked)," local images OK]")

            db.db = np.asarray(checked)
        else:
            # check online images
            pass
