
def toStringDigit(number, digit):
    snumber = str(number)
    return '0'*(digit-len(snumber)) +snumber

def checkString(s,dictConfig):
    res = []
    begin=0
    # prefix
    prefixes = dictConfig['prefix']
    for p in prefixes:
        lenp =len(p)
        if s[0:lenp]==p:
            res.append(True)
            begin = begin + lenp
            break
    
    # digit
    nbDigit = int(dictConfig['numberDigit'])
    sDigit =s[begin:begin+nbDigit]
    if sDigit.isdigit():
        res.append(True)
        begin = begin + nbDigit

    # suffix
    suffixes = dictConfig['suffix']
    for su in suffixes:
        lens =len(su)
        if s[begin:begin+lens]==su:
            res.append(True)
            begin =  begin + lens

    # dot
    if s[begin:begin+1]== '.':
        res.append(True)
        begin = begin+1

    # ext
    exts = dictConfig['ext']
    for e in exts:
        if e == s[begin:]:
            res.append(True)
            break

    if len(res) == 5:
        return True
    else:
        return False

def filename_to_number(s,dictConfig):
    res = None
    # prefix
    prefix = dictConfig['prefix'][0]
    lenp =len(prefix)

    # digit
    nbDigit = int(dictConfig['numberDigit'])
    sDigit =s[lenp:lenp+nbDigit]

    if sDigit.isdigit():
        res = int(sDigit)

    return res

def number_to_filenames(num,dictConfig):
    res = []

    # prefix
    prefixes = dictConfig['prefix']
    for p in prefixes:
        res.append(dictConfig['path']+p)
    
    # digit and expo
    nbDigit = int(dictConfig['numberDigit'])
    expo = dictConfig['expo']

    newres = []
    for f in res:
        for e in expo:
            expoNum = int(e)
            sDigit =toStringDigit(num+expoNum, nbDigit)
            newres.append(f+sDigit)
    res = newres

    # suffix
    suffixes = dictConfig['suffix']
    newres = []
    for f in res:
        for su in suffixes: newres.append(f+su)
    res = newres

    # dot
    newres = []
    for f in res: newres.append(f+'.')
    res= newres

    # ext
    exts = dictConfig['ext']
    newres = []
    for f in res:
       for e in exts: newres.append(f+e)
    res = newres

    return res

def img_url_2_file_ext(imagePath,url,minRange=-3,maxRange=-1):
    dat = url.split('/')[minRange:maxRange]     # split url file name (/) and keep the part between minRange and maxRange
    retStr = url.split("/")[-1].split(".")      # last element id the image file name
    retStr = retStr[0:-1]
    file = '.'.join(retStr)
    ext = url.split("/")[-1].split(".")[-1]     # image extension
    retStr = dat+[file]
    retStr = '_'.join(retStr)
    retStr = imagePath+"/"+retStr
    return retStr,ext
