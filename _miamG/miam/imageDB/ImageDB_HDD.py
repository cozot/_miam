from . import ImageDB

import os, json, functools, datetime, imageio
import numpy as np
import matplotlib.pyplot as plt

import miam.html.generator as MHTML
import miam.image.Image as MIMG
import miam.histogram.Histogram as MHIST
import miam.processing.TMO_CCTF

class ImageDB_HDD(ImageDB.ImageDB):
    """description of class"""
    def __init__(self,name=None, csvFile=None, jsonConfigFile = None):
        super().__init__(name, csvFile, jsonConfigFile)

    def check(self,uri2file=None):
        checked = []
        missingItems = []
        missingFiles = []

        # load config
        with open(self.jsonConfigFile) as json_file: config = json.load(json_file)
        path = config["imagePATH"]


        # for all item in db
        for item in self.db:
            itemOk = list(map(lambda x : os.path.isfile(path+x), item))
            ok = functools.reduce(lambda x,y : x and y,itemOk)
            if ok:
                checked.append(item)
            else:
                missingItems.append(item)
                for i, fileOk in enumerate(itemOk):
                    if not fileOk: missingFiles.append(item[i])
        # results
        path = self.csvFileName[0: len(self.csvFileName) - len(self.csvFileName.split("/")[-1])]
        np.savetxt(path+name+"_"+"missingItems.csv",missingItems,delimiter=";",fmt="%s",encoding="utf8")
        np.savetxt(path+name+"_"+"missingImages.csv",missingFiles,delimiter=";",fmt="%s",encoding="utf8")

        self.db = np.asarray(checked)

    def buildHTMLPage(self):
        # get date
        todayString = datetime.date.today().strftime("%Y/%m/%d")

        # load config
        with open(self.jsonConfigFile) as json_file: config = json.load(json_file)
        path = config["imagePATH"]

        # <!DOCTYPE html>
        # <html>
        # <head><title>HDR databse</title></head>
        # <BODY>
        # <H1>HDR DataBase</H1>
        # </BODY>
        # </html>

        head = MHTML.generator.tag('head', content=MHTML.generator.tag('title', content='hdr database'))
        h1 = MHTML.generator.tag('h1', content='HDR DATABASE')
        date = MHTML.generator.tag('h3', content='Rémi Cozot - '+ todayString)
        pageBody =''
        # for all items
        for item in self.db:

            # load HDR image
            hdr =  MIMG.Image.readImage(path+item[0]).resize((None,480)).removeZeros().removeZeros(0.5)
            # basic tone mapping
            hdrCCTF = hdr.process(miam.processing.TMO_CCTF.TMO_CCTF(), function='sRGB')
            # read Hand Tone Mapped image
            image = MIMG.Image.readImage(path+item[-1]).resize((None,480))

            # compute Y(HDR) and L(SDR) histograms
            hdrhistoY = MHIST.Histogram.build(hdr,MIMG.channel.channel.Y, nbBins=50)
            imagehistoL = MHIST.Histogram.build(image,MIMG.channel.channel.L, nbBins=50)
            
            # create name
            imageName = (item[-1].split('/')[-1]).split(".")[0]

            # create and save histogram images
            fig, ax = plt.subplots()
            hdrhistoY.plot(ax)
            plt.show(block=False)
            plt.savefig ( "../HTML/images/"+"HDR_"+imageName+"_YHIST.png" )
            plt.close(fig)
            
            fig, ax = plt.subplots()
            imagehistoL.plot(ax)
            plt.show(block=False)
            plt.savefig ( "../HTML/images/"+"SDR_"+imageName+"_LHIST.png" )    
            plt.close(fig)

            # save CCTF and HTM images
            imageio.imwrite('../HTML/images/'+"HTM_"+imageName+".jpg", (image.colorData*256).astype(np.uint8))
            imageio.imwrite('../HTML/images/'+"CCTF_"+imageName+".jpg", (hdrCCTF.colorData*256).astype(np.uint8))

            # uri for web page
            name_CCTF_image = "images/"+"CCTF_"+imageName+".jpg"
            name_HTM_image  = "images/"+"HTM_"+imageName+".jpg"
            name_HDR_Yhist  = "images/"+"HDR_"+imageName+"_YHIST.png"
            name_SDR_Lhist  = "images/"+"SDR_"+imageName+"_LHIST.png"

            # create <img> table
            pageBody = pageBody +   MHTML.generator.tag('h2', imageName)+'\n' + \
                                    MHTML.generator.table([[ MHTML.generator.imgTagWidth(attributeValue=[name_CCTF_image,'50%']),MHTML.generator.imgTagWidth(attributeValue=[name_HDR_Yhist,'50%'])], 
                                                           [MHTML.generator.imgTagWidth(attributeValue=[name_HTM_image,'50%']),MHTML.generator.imgTagWidth(attributeValue=[name_SDR_Lhist,'50%'])]])+ '\n'
            
            print(">",imageName)
            # end loop
        all =  MHTML.generator.tag('html', content=     head + '\n' + \
                                                        MHTML.generator.tag('body', content= h1 + '\n' + date + '\n' + \
                                                        pageBody
                                  ))

        fileHTML = open("../HTML/hdr-database.html","w") 
        fileHTML.write(all) 
        fileHTML.close() 
 



