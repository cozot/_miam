# import
# ------------------------------------------------------------------------------------------
import miam
import copy
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class Distance(object):
    """description of class"""
    def __init__(self,f):
        self.distanceFunction = f

    def eval(self, u,v): return self.distanceFunction(u,v)

def cosineDistance(u,v):
	# normalise u, v
	u = u / np.sqrt(np.dot(u,u))
	v = v / np.sqrt(np.dot(v,v))
	# cosine
	uv = np.dot(u,v)
	# return
	return np.maximum(1 - uv, 0.0)

def L2Distance(u,v):
    u = np.asarray(u)
    v = np.asarray(v)
    # return
    return np.sqrt(np.dot(u-v,u-v))

def cL2distance(c0,c1):
    # sorted L2 distanec between palette.colors
    # no border effect
    c0, c1 = copy.deepcopy(c0), copy.deepcopy(c1)

    # init iteration
    totalDist = 0.0

    while (len(c0)>0):

        # init find mininal distance between two colors
        u, v = c0[0], c1[0]
        uMv = u-v
        distMin = np.sqrt(np.dot(uMv,uMv))
        iMin, jMin =0, 0

        for i in range(len(c0)):
            for j in range(len(c1)):
                u, v = c0[i], c1[j]
                uMv = u-v
                dist = np.sqrt(np.dot(uMv,uMv))

                if dist < distMin:
                    distMin, iMin, jMin = dist,i,j
                else:
                    pass
        # remove colors that are closest
        c0, c1 = np.delete(c0,iMin, axis=0), np.delete(c1,jMin, axis=0)
        # add to distance
        totalDist = totalDist + distMin
    return totalDist

