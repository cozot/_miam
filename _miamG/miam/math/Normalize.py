# import
# ------------------------------------------------------------------------------------------
import miam
import copy
import numpy as np
# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

class Normalize(object):
	"""description of class"""
	def __init__(self,f):
		self.normFunction = f

	def eval(self, u):  return self.normFunction(u)	# single vector

	def evals(self, u):								# array of vector 
		res = np.zeros(u.shape)								
		for i in range(u.shape[0]): res[i] = self.normFunction(u[i])

		return res

def cosineNorm(u):
	u1 = copy.deepcopy(u)

	unorm = np.sqrt(np.dot(u1,u1))
	u1 =u1/unorm if unorm != 0.0 else u1

	return u1

def noNorm(u): return u

def sortNorm(u):
	u1 = copy.deepcopy(u)
	return np.asarray(sorted(u1.tolist(), key = lambda u  : np.sqrt(np.dot(u,u))))
