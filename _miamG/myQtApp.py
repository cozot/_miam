# import
# ------------------------------------------------------------------------------------------
import os, sys, math
import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import easygui
import colour

# import Qt
from PyQt5.QtWidgets import QMainWindow, QAction, QApplication, QMenu
from PyQt5.QtWidgets import QWidget, QLabel, QFileDialog 
from PyQt5.QtWidgets import QHBoxLayout # QSlider
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5 import QtCore

# miam import
import miam.image.Image as MIMG
import miam.aesthetics.Palette as MPAL
import miam.utils

# new import
import gui.guiView.ImageWidget as gIW

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

def getScreenSize():
    app = QApplication(sys.argv)
    screens = app.screens()

    for s in screens:
        n = s.name()
        size = s.size()

        x = size.width()
        y = size.height()

        print("screen(",n,"):",x,"x",y)

    app.quit()

    return None


def testQt():
    getScreenSize()
    pass

class QMainApp(QMainWindow):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
    def initUI(self):         
        
        menubar = self.menuBar()
        # file menu
        fileMenu = menubar.addMenu('&File')

        # Create Open action
        openAction = QAction('&Open', self)        
        openAction.setShortcut('Ctrl+N')
        openAction.setStatusTip('Open image')
        openAction.triggered.connect(self._open)
        
        fileMenu.addAction(openAction)

        # Create Save action
        saveAction = QAction('&Save', self)        
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save image')
        saveAction.triggered.connect(self._save)
        
        fileMenu.addAction(saveAction)

        # Create Quit action
        quitAction = QAction('&Quit', self)        
        quitAction.setShortcut('Alt+F4')
        quitAction.setStatusTip('Quit')
        quitAction.triggered.connect(self._quit)
        
        fileMenu.addAction(quitAction)

        # status bar
        self.statusBar().showMessage('Welcome to MIAM: Multidimensional Image Aesthetics Model!')

        # geometry
        base, ratio, scale= 1920, 16/9, 0.5
        self.setGeometry(0, 0, math.floor(base*scale), math.floor(base/ratio*scale))

        # title
        self.setWindowTitle('miam')    

        #
        self.img0 = gIW.ImageWidget(None,filename= '../images/DSC01314-HDR.jpg', mother=self)
        self.img1 = gIW.ImageWidget(None,filename= '../images/DSC01314.JPG', mother=self)
        imgLayout = QHBoxLayout()
        imgLayout.addWidget(self.img0)
        imgLayout.addWidget(self.img1)

        widget = QWidget()
        widget.setLayout(imgLayout)
        self.setCentralWidget(widget)

        # show
        self.show()

    def _statusMessage(self, s): self.statusBar().showMessage(s)

    def _open(self):
        self._statusMessage('open!!')
        fname = QFileDialog.getOpenFileName(self, 'Open file', '../images/')
        print(fname[0])
        self.centralWidget()._new(fname[0])

    def _save(self):
        self._statusMessage('save!!')

    def _quit(self):
        self._statusMessage('quit!!')
        sys.exit()

    def resizeEvent(self, event):

        self.img0._resize()
        self.img1._resize()
        pass


def qtTest2():
    app = QApplication(sys.argv)
    ex = QMainApp()
    sys.exit(app.exec_())