# import
# ------------------------------------------------------------------------------------------
import os
import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import easygui
import colour

# miam import
import miam.image.Image as MIMG
import miam.processing.TMO_Lightness as TMO_L
import miam.utils

# ------------------------------------------------------------------------------------------
# MIAM project 2020
# ------------------------------------------------------------------------------------------
# author: remi.cozot@univ-littoral.fr
# ------------------------------------------------------------------------------------------

def pfun(f):
    path, name, ext = miam.utils.splitFileName(f)

    current = mp.current_process()
    print('core:', current.name,"(",current._identity,")","::",name)

    # load image and remove zeros
    img = MIMG.Image.readImage(f).removeZeros().removeZeros(0.5)
    img.name = name   

    # tonemap
    tmImg = img.process(TMO_L.TMO_Lightness())
    tmImg.name = img.name+"_Lightness_TMO"

    # sve tone mapped image
    tmImg.writeImage("../lightness_TMO/"+tmImg.name)

    print('core:', current.name,"(",current._identity,")","::",tmImg.name,":: done n saved")

    return None

if __name__ == '__main__':

    print("MIAM: lightness tone all hdr images")

    nbCpu = mp.cpu_count()
    print(" ---- found:", nbCpu, "cpu cores")

    srcPath = "../../../photos/HDR-DB/HDR/"
    hdrfiles = os.listdir(srcPath)
    todo = []

    respath = "../lightness_TMO/"
    for h in hdrfiles:
        path, name, ext = miam.utils.splitFileName(h)
        jfile = respath+name+"_Lightness_TMO.json"
        tmofile = respath+name+"_Lightness_TMO.jpg"
        jfileOk = os.path.isfile(jfile)
        tmofileOK = os.path.isfile(tmofile)
        if ((not jfileOk) or (not tmofileOK)): todo.append(h)

    listFiles = list(map(lambda x : srcPath+x, todo))


    print(" ---- number of images:",len(listFiles))
    for f in listFiles: print(" ---- file:", f)
    print(" ---- launch tone mapping !")

    _pool = mp.Pool()
    result = _pool.map(pfun, listFiles)
    print(" ---- tone mapping all done !")
